#pragma once
#include "EntityKernel.h"

class OrbitNoteObject
{
public:
	OrbitNoteObject(std::string name, MyVector3 position, OrbitingEntityKernel *kernel = 0, double radius = 0)
	{
		_name = name;
		_position = position;
		_kernel = kernel;
		_radius = radius;
	}

	double _radius;
	std::string _name;
	MyVector3 _position;
	OrbitingEntityKernel * _kernel;
};

