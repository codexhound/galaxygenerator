#pragma once
#include "EntityKernel.h"

class Window;
class BaseApplication;

namespace CEGUI 
{
	class Window;
	class ToggleButton;
	class Combobox;
	class Spinner;
}

class ChildWindow
{
public:
	ChildWindow(CEGUI::Window * parent) { _parent = parent; _thisWindow = 0; }

	CEGUI::Window * _parent;
	CEGUI::Window * GetThisWindow() { return _thisWindow; }

	virtual void UpdateInfo() = 0;
	virtual void ResetInfo() = 0;

protected:
	CEGUI::Window * _thisWindow;
};

class ObjectDetailsWindow : public ChildWindow
{
public:
	ObjectDetailsWindow(CEGUI::Window * parent);

	//Generic
	CEGUI::Window * _nameLabel;
	CEGUI::Window * _typeLabel;
	CEGUI::Window * _positionLabel;
	CEGUI::Window * _nearestNeighbor;
	CEGUI::Window * _cameraDistance;
	
	//Orbiting Type
	void RenderOrbitingType(EntityKernel * kernel);
	CEGUI::Window * _orbitParentName;
	CEGUI::Window * _orbitDistance;
	CEGUI::Window * _orbitVelocity;
	CEGUI::Window * _centralEntity;
	CEGUI::Window * _orbitParentType;
	CEGUI::ToggleButton * _displayOrbitPath;
	
	//Central Type
	void RenderCentralType(EntityKernel * kernel);
	CEGUI::Window * _systemNearestNeighbor;
	CEGUI::Window * _systemPosition;
	CEGUI::Window * _numberOfOrbitingObjects;
	CEGUI::ToggleButton * _displayOrbits;

	virtual void UpdateInfo();
	virtual void ResetInfo();
	void HideAllNonGeneric();

	void SelectionChanged(EntityKernel * kernel);

	entitykernel::TypeDefs type;

private:
	void UpdateGeneric();
	void UpdateOrbitInfo();
	void UpdateSystemInfo();
	std::string GetNearestNeighborInfo(BaseEntityKernel * kernel);
	void RenderCameraDistance(EntityKernel * kernel);

	float drawInc;
	float ySize;
};

class TopBarWindow : public ChildWindow
{
	public:
		TopBarWindow(CEGUI::Window * parent);

		CEGUI::Window * _pause;
		CEGUI::Window * _quit;
		CEGUI::Window * _toggleRenderOrbit;
		CEGUI::Window * _increaseSpeed;
		CEGUI::Window * _decreaseSpeed;
		CEGUI::Window * _speedInfo;
		CEGUI::Window * _yearsInfo;
		CEGUI::Window * _moveSpeedInfo;
		CEGUI::ToggleButton * _toggleTrails;

		virtual void UpdateInfo();
		virtual void ResetInfo();
};

/*
class Menu : public ChildWindow
{
public:
	Menu(CEGUI::Window * parent) : ChildWindow(parent) {}

	CEGUI::Window * _closeButton;
};*/

class MainMenu : public ChildWindow
{
public:
	MainMenu(CEGUI::Window * parent);

	std::map<std::string, Vector2> availableRes;

	virtual void UpdateInfo();
	virtual void ResetInfo();

	CEGUI::Combobox * _resolution;
	CEGUI::Combobox * _fullscreen;
	CEGUI::Window * _quit;
	CEGUI::Window * _apply;
	CEGUI::Window * _cancel;
	CEGUI::Window * _generateButton;

	void CancelSettings();
	void ApplySettings();

	bool fullscreen;
	std::string curResKey;

	private:
		//cur settings	
};

class GenWindow : public ChildWindow
{
public:
	GenWindow(CEGUI::Window * parent);

	virtual void UpdateInfo();
	virtual void ResetInfo();

	CEGUI::Spinner * _setNumStars;
	CEGUI::Spinner * _setRecursionDepth;
	CEGUI::Spinner * _setNumPlacementTries;
	CEGUI::Spinner * _setGalaxySize;

	CEGUI::Window * _generateButton;
	CEGUI::Window * _closeButton;

	void GeneratePressed();
};

class WindowManager
{
	friend class BaseApplication;
	public:
		static void IntitializeWindows(BaseApplication * mainApplication);
		~WindowManager();

		static TopBarWindow * topBar;
		static ObjectDetailsWindow * details;
		static MainMenu * _menuWindow;
		static CEGUI::Window * _loadingWindow;
		static CEGUI::Window * _loadingText;
		static GenWindow * _generateGalaxyOptions;

		static CEGUI::Window * _mainWindow;
		static BaseApplication * _mainApp;

		static void UpdateInfo();
		static void ResetInfo();

		static void ShowLoadingProgress();
		static void HideLoadingProgress();
		static void ShowMenu();
		static void HideMenu();
		static void HideUI();
		static void ShowUI();
		static void HideTopBar();
		static void ShowTopBar();
		static void ShowGenerateGalaxyOptions();
		static void HideGenerateGalaxyOptions();

		static void ShowDetails();
		static void HideDetails();

		static void SelectionChanged(EntityKernel * kernel);

	private:
		WindowManager();

		static void InitTopBar();
};

