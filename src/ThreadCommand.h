#pragma once

class OrbitingEntityKernel;

class ThreadCommand
{
public:
	ThreadCommand() {}

	virtual void Process() = 0;
};

class OrbitRender : public ThreadCommand
{
public:
	OrbitRender() : ThreadCommand()
	{
		render = false;
		centralEntity = 0;
	}

	virtual void Process();

	bool render;
	OrbitingEntityKernel * centralEntity;
};

class OrbitPathRender : public ThreadCommand
{
public:
	OrbitPathRender() : ThreadCommand()
	{
		render = false;
		entity = 0;
	}

	virtual void Process();

	bool render;
	OrbitingEntityKernel * entity;
};

class StopRenderingAllOrbits : public ThreadCommand
{
public:
	StopRenderingAllOrbits() : ThreadCommand() {}

	virtual void Process();
};

