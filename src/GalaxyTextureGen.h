#pragma once
#include <vector>
#include <OgreEntity.h>
#include "EntityKernel.h"

namespace Verse
{
	class GalaxyTextureGen
	{
		public:
			~GalaxyTextureGen() {}
			
			static Ogre::MaterialPtr GeneratePlanetMaterial(std::string name);
			static Ogre::MaterialPtr GenerateStarMaterial(std::string name);
			static Ogre::MaterialPtr GenerateBlackHoleMaterial(std::string name);
			static Ogre::MaterialPtr GetRandomMaterial(EntityType type);

			static void GenerateMaterials();
			static void ResetMaterials();
			static bool MaterialsExist();

			static std::vector<Ogre::MaterialPtr> _planetMaterialList;
			static std::vector<Ogre::MaterialPtr> _starMaterialList;

		private:
			GalaxyTextureGen();

			static int _amountofMaterialsToGenerateEach;
	};
}
