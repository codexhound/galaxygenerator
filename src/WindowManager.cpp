#include "WindowManager.h"
#include "BaseApplication.h"
#include "CEGUI/widgets/All.h"
#include "EntityKernelManager.h"
#include "SystemEntityKernel.h"
#include <iostream>
#include "MathDefines.h"

TopBarWindow::TopBarWindow(CEGUI::Window * parent) : ChildWindow(parent)
{
	BaseApplication * mainApp = WindowManager::_mainApp;
	if(_parent != NULL && mainApp != NULL)
	{
		CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
	
		_thisWindow = wmgr.createWindow("DefaultWindow", "TopBar");
		_thisWindow->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(0.2f, 0.0f)));
		_parent->addChild(_thisWindow);
	
		//quit button
		_quit = wmgr.createWindow("TaharezLook/Button", "TopBar/QuitButton");
		_quit->setText("Quit");
		_quit->setSize(CEGUI::USize(CEGUI::UDim(0.15f, 0.0f), CEGUI::UDim(0.5, 0.0f)));
		_thisWindow->addChild(_quit);
		_quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::quit, mainApp));
	
		//pause button
		_pause = wmgr.createWindow("TaharezLook/Button", "TopBar/PauseButton");
		_pause->setText("Pause/Unpause Sim");
		_pause->setPosition(CEGUI::UVector2(CEGUI::UDim(0.15f, 0.0f), CEGUI::UDim(0.0f, 0.f)));
		_pause->setSize(CEGUI::USize(CEGUI::UDim(0.15f, 0.0f), CEGUI::UDim(0.5f, 0.0f)));
		_thisWindow->addChild(_pause);
		_pause->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::pause, mainApp));
	
		//render orbits button
		_toggleRenderOrbit = wmgr.createWindow("TaharezLook/Button", "TopBar/ToggleRenderOrbits");
		_toggleRenderOrbit->setText("Stop Rendering All Orbits");
		_toggleRenderOrbit->setPosition(CEGUI::UVector2(CEGUI::UDim(0.30f, 0), CEGUI::UDim(0.0f, 0.0f)));
		_toggleRenderOrbit->setSize(CEGUI::USize(CEGUI::UDim(0.25f, 0.0f), CEGUI::UDim(0.5, 0.0f)));
		_thisWindow->addChild(_toggleRenderOrbit);
		_toggleRenderOrbit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::stoprenderorbits, mainApp));
	
		//Increase Sim Speed
		_increaseSpeed = wmgr.createWindow("TaharezLook/Button", "TopBar/+SpeedButton");
		_increaseSpeed->setText("Increase Speed");
		_increaseSpeed->setPosition(CEGUI::UVector2(CEGUI::UDim(0.55f, 0.0f), CEGUI::UDim(0.0f, 0.0f)));
		_increaseSpeed->setSize(CEGUI::USize(CEGUI::UDim(0.2f, 0.0f), CEGUI::UDim(0.5f, 0.0f)));
		_thisWindow->addChild(_increaseSpeed);
		_increaseSpeed->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::changesimspeed, mainApp));
	
		//Decrease Sim Speed
		_decreaseSpeed = wmgr.createWindow("TaharezLook/Button", "TopBar/-SpeedButton");
		_decreaseSpeed->setText("Decrease Speed");
		_decreaseSpeed->setPosition(CEGUI::UVector2(CEGUI::UDim(0.75f, 0.0f), CEGUI::UDim(0.0f, 0.0f)));
		_decreaseSpeed->setSize(CEGUI::USize(CEGUI::UDim(0.2f, 0.0f), CEGUI::UDim(0.5f, 0.0f)));
		_thisWindow->addChild(_decreaseSpeed);
		_decreaseSpeed->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::changesimspeed, mainApp));
	
		//render trails button
		CEGUI::Window* trailsToggle = wmgr.createWindow("TaharezLook/Checkbox", "TopBar/ToggleRenderTrails");
		_toggleTrails = dynamic_cast<CEGUI::ToggleButton*>(trailsToggle);
		if (_toggleRenderOrbit != NULL)
		{
			_toggleTrails->setSelected(false);
			_toggleTrails->setText("Render Trails?");
			_toggleTrails->setPosition(CEGUI::UVector2(CEGUI::UDim(0.32f, 0.0f), CEGUI::UDim(0.5f, 0.0f)));
			_toggleTrails->setSize(CEGUI::USize(CEGUI::UDim(0.25f, 0.0f), CEGUI::UDim(0.5f, 0.0f)));
			_thisWindow->addChild(_toggleTrails);
			_toggleTrails->subscribeEvent(CEGUI::ToggleButton::EventSelectStateChanged, CEGUI::Event::Subscriber(&BaseApplication::renderTrails, mainApp));
		}

		//Info Display
		CEGUI::Window * infoDisplay = wmgr.createWindow("DefaultWindow", "TopBar/InfoDisplay");
		infoDisplay->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5f, 0.0f), CEGUI::UDim(0.5f, 0.0f)));
		infoDisplay->setSize(CEGUI::USize(CEGUI::UDim(0.4f, 0.0f), CEGUI::UDim(0.5, 0.0f)));
		_thisWindow->addChild(infoDisplay);
		_speedInfo = wmgr.createWindow("TaharezLook/Label", "TopBar/InfoDisplay/Speed");
		_speedInfo->setText("Speed Factor: " + std::to_string(1.0));
		_speedInfo->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0)));
		_speedInfo->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(0.2f, 0.0f)));
		_yearsInfo = wmgr.createWindow("TaharezLook/Label", "TopBar/InfoDisplay/Years");
		_yearsInfo->setText("Years Elapsed: " + std::to_string(0.0));
		_yearsInfo->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(0.2f, 0)));
		_yearsInfo->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(0.2f, 0.0f)));
		_moveSpeedInfo = wmgr.createWindow("TaharezLook/Label", "TopBar/InfoDisplay/MoveSpeed");
		_moveSpeedInfo->setText("Current Move Speed: " + std::to_string(WindowManager::_mainApp->mSpeed));
		_moveSpeedInfo->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(0.4f, 0.0f)));
		_moveSpeedInfo->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(0.2f, 0.0f)));
		infoDisplay->addChild(_speedInfo);
		infoDisplay->addChild(_yearsInfo);
		infoDisplay->addChild(_moveSpeedInfo);
	}	
	
}

void TopBarWindow::UpdateInfo()
{
	double speedFactor = EntityKernelManager::_gameSpeedFactor;
	_speedInfo->setText("Speed Factor: " + std::to_string(speedFactor));

	_yearsInfo->setText("Years Elapsed: " + std::to_string(EntityKernelManager::_gameYearsElapsed));
	_moveSpeedInfo->setText("Current Move Speed: " + std::to_string(WindowManager::_mainApp->mSpeed));
}

void TopBarWindow::ResetInfo()
{
	_speedInfo->setText("Speed Factor: " + std::to_string(1.0));
	_yearsInfo->setText("Years Elapsed: " + std::to_string(0.0));
	_moveSpeedInfo->setText("Current Move Speed: " + std::to_string(100.0));
	_toggleTrails->setSelected(false);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

ObjectDetailsWindow::ObjectDetailsWindow(CEGUI::Window * parent) : ChildWindow(parent)
{
	type = NOTYPE;
	float curDrawPos = 0;
	drawInc = 0.05;
	ySize = 0.05;
	BaseApplication * mainApp = WindowManager::_mainApp;
	if (_parent != NULL && mainApp != NULL)
	{
		CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
		_thisWindow = wmgr.createWindow("TaharezLook/GroupBox", "DetailsPane");
		CEGUI::GroupBox * detailsPane = dynamic_cast<CEGUI::GroupBox*>(_thisWindow);
		if (detailsPane != NULL)
		{
			//detailsPane->setProperty("Horizontal")
		}
		_thisWindow->setSize(CEGUI::USize(CEGUI::UDim(0.7f, 0.0f), CEGUI::UDim(0.5f, 0.0f)));
		_thisWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(0.6f, 0.0f)));
		_parent->addChild(_thisWindow);

		_nameLabel = wmgr.createWindow("TaharezLook/Label", "Details/NameLabel");
		_nameLabel->setText("NULL");
		_nameLabel->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(ySize, 0.0f)));
		_thisWindow->addChild(_nameLabel);
		curDrawPos += drawInc;

		_typeLabel = wmgr.createWindow("TaharezLook/Label", "Details/typeLabel");
		_typeLabel->setText("NULL");
		_typeLabel->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(ySize, 0.0f)));
		_typeLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(curDrawPos, 0.0f)));
		_thisWindow->addChild(_typeLabel);
		curDrawPos += drawInc;

		_cameraDistance = wmgr.createWindow("TaharezLook/Label", "Details/typeLabel");
		_cameraDistance->setText("NULL");
		_cameraDistance->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(ySize, 0.0f)));
		_cameraDistance->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(curDrawPos, 0.0f)));
		_thisWindow->addChild(_cameraDistance);
		curDrawPos += drawInc;

		_centralEntity = wmgr.createWindow("TaharezLook/Label", "Details/isCentralLabel");
		_centralEntity->setText("NULL");
		_centralEntity->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(ySize, 0.0f)));
		_centralEntity->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(curDrawPos, 0.0f)));
		_thisWindow->addChild(_centralEntity);
		curDrawPos += drawInc;

		_positionLabel = wmgr.createWindow("TaharezLook/Label", "Details/pos");
		_positionLabel->setText("NULL");
		_positionLabel->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(ySize, 0.0f)));
		_positionLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(curDrawPos, 0.0f)));
		_thisWindow->addChild(_positionLabel);
		curDrawPos += drawInc;

		_nearestNeighbor = wmgr.createWindow("TaharezLook/Label", "Details/nearestNeighbor");
		_nearestNeighbor->setText("NULL");
		_nearestNeighbor->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f), CEGUI::UDim(ySize, 0.0f)));
		_nearestNeighbor->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(curDrawPos, 0.0f)));
		_thisWindow->addChild(_nearestNeighbor);
		curDrawPos += drawInc;

		_orbitParentName = wmgr.createWindow("TaharezLook/Label", "Details/orbitParentName");
		_orbitParentName->setText("NULL");
		_orbitParentName->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_orbitParentName->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0)));
		_thisWindow->addChild(_orbitParentName);
		curDrawPos += drawInc;

		_orbitParentType = wmgr.createWindow("TaharezLook/Label", "Details/orbitParentType");
		_orbitParentType->setText("NULL");
		_orbitParentType->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_orbitParentType->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0)));
		_thisWindow->addChild(_orbitParentType);
		curDrawPos += drawInc;

		_orbitDistance = wmgr.createWindow("TaharezLook/Label", "Details/orbitDistance");
		_orbitDistance->setText("NULL");
		_orbitDistance->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_orbitDistance->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0.0)));
		_thisWindow->addChild(_orbitDistance);
		curDrawPos += drawInc;

		_orbitVelocity = wmgr.createWindow("TaharezLook/Label", "Details/orbitVelocity");
		_orbitVelocity->setText("NULL");
		_orbitVelocity->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_orbitVelocity->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0)));
		_thisWindow->addChild(_orbitVelocity);
		curDrawPos += drawInc;

		CEGUI::Window * displayOrbit = wmgr.createWindow("TaharezLook/Checkbox", "Details/displayOrbitPath");
		_displayOrbitPath = static_cast<CEGUI::ToggleButton*>(displayOrbit);
		_displayOrbitPath->setText("Dispay Orbit Path");
		_displayOrbitPath->setSelected(false);
		_displayOrbitPath->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_displayOrbitPath->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0)));
		_displayOrbitPath->subscribeEvent(CEGUI::ToggleButton::EventSelectStateChanged, CEGUI::Event::Subscriber(&BaseApplication::renderPathChanged, mainApp));
		_thisWindow->addChild(_displayOrbitPath);
		curDrawPos += drawInc;

		_systemPosition = wmgr.createWindow("TaharezLook/Label", "Details/systemPosition");
		_systemPosition->setText("NULL");
		_systemPosition->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_systemPosition->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0.0)));
		_thisWindow->addChild(_systemPosition);
		curDrawPos += drawInc;

		_systemNearestNeighbor = wmgr.createWindow("TaharezLook/Label", "Details/systemNearestNeighbor");
		_systemNearestNeighbor->setText("NULL");
		_systemNearestNeighbor->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_systemNearestNeighbor->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0.0)));
		_thisWindow->addChild(_systemNearestNeighbor);
		curDrawPos += drawInc;

		_numberOfOrbitingObjects = wmgr.createWindow("TaharezLook/Label", "Details/numberOfOrbitingObjects");
		_numberOfOrbitingObjects->setText("NULL");
		_numberOfOrbitingObjects->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_numberOfOrbitingObjects->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0.0)));
		_thisWindow->addChild(_numberOfOrbitingObjects);
		curDrawPos += drawInc;

		CEGUI::Window * displayOrbits = wmgr.createWindow("TaharezLook/Checkbox", "Details/displayOrbits");
		_displayOrbits = static_cast<CEGUI::ToggleButton*>(displayOrbits);
		_displayOrbits->setSelected(false);
		_displayOrbits->setText("Display System Orbit Paths");
		_displayOrbits->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(ySize, 0)));
		_displayOrbits->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curDrawPos, 0)));
		_displayOrbits->subscribeEvent(CEGUI::ToggleButton::EventSelectStateChanged, CEGUI::Event::Subscriber(&BaseApplication::renderOrbitsChanged, mainApp));
		_thisWindow->addChild(_displayOrbits);
		curDrawPos += drawInc;
	}

	SelectionChanged(NULL);
}

void ObjectDetailsWindow::UpdateInfo()
{
	UpdateGeneric();
	UpdateOrbitInfo();
	UpdateSystemInfo();
}

void ObjectDetailsWindow::ResetInfo()
{
	SelectionChanged(NULL);
}

void ObjectDetailsWindow::SelectionChanged(EntityKernel * kernel)
{
	BaseApplication * mainApp = WindowManager::_mainApp;
	if (kernel != NULL && _parent && mainApp != NULL)
	{
		HideAllNonGeneric(); //hide non referenced child windows
		//display generic details first
		CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
		//name label
		_nameLabel->setText("Name: " + kernel->GetName());

		std::string type = entitykernel::ConvertEntityTypeToString(kernel->GetType());
		_typeLabel->setText("Object Type: " + type);

		RenderCameraDistance(kernel);

		Vector3 position = kernel->GetPosition();
		_positionLabel->setText("Position (System Relative): X-> " + std::to_string(position[0]) + ", Y-> " + std::to_string(position[1]) + ", Z-> " + std::to_string(position[2]));

		std::string nearestNeighborText = GetNearestNeighborInfo(kernel);
		if (nearestNeighborText != "Not Available")
		{
			nearestNeighborText = "Nearest Neighbor Name: " + nearestNeighborText;
		}
		_nearestNeighbor->setText(nearestNeighborText);
		
		RenderOrbitingType(kernel);
		RenderCentralType(kernel);

		WindowManager::ShowDetails();
	}
	else WindowManager::HideDetails();
}
void ObjectDetailsWindow::RenderCameraDistance(EntityKernel * kernel)
{
	SceneNode * kernelSceneNode = kernel->GetRendererNode();
	double cameraDistance = 0;
	if (kernelSceneNode != NULL)
	{
		cameraDistance = WindowManager::_mainApp->mCameraNode->getPosition().distance(kernelSceneNode->getPosition());
	}
	_cameraDistance->setText("Distance From Camera: " + std::to_string(cameraDistance));
}


std::string ObjectDetailsWindow::GetNearestNeighborInfo(BaseEntityKernel * kernel)
{
	std::string type = "Not Available";
	std::string nearestNeighborName = "Not Available";
	BaseEntityKernel * nnEntity = 0;
	double nnDistance = 0;
	std::string text = "Not Available";
	nnEntity = kernel->GetNearestNeighbor();
	if (nnEntity != NULL)
	{
		nearestNeighborName = nnEntity->GetName();
		nnDistance = kernel->GetPosition().distance(nnEntity->GetPosition());
		type = entitykernel::ConvertEntityTypeToString(nnEntity->GetType());
		text = nearestNeighborName + ", Type: " + type + ", Distance: " + std::to_string(nnDistance);
	}
	return text;
}

void ObjectDetailsWindow::UpdateGeneric()
{
	EntityKernel * kernel = WindowManager::_mainApp->currentDetailsObject;
	if (kernel != NULL)
	{
		MyVector3 position = kernel->GetPosition();
		_positionLabel->setText("Position (System Relative): X-> " + std::to_string(position[0]) + ", Y-> " + std::to_string(position[1]) + ", Z-> " + std::to_string(position[2]));
		std::string nearestNeighborText = GetNearestNeighborInfo(kernel);
		if (nearestNeighborText != "Not Available")
		{
			nearestNeighborText = "Nearest Neighbor Name: " + nearestNeighborText;
		}
		_nearestNeighbor->setText(nearestNeighborText);
		RenderCameraDistance(kernel);
	}
}

void ObjectDetailsWindow::UpdateOrbitInfo()
{
	EntityKernel * kernel = WindowManager::_mainApp->currentDetailsObject;
	if (kernel != NULL)
	{
		OrbitingEntityKernel * orbitKernel = dynamic_cast<OrbitingEntityKernel *>(kernel);
		if (orbitKernel != NULL)
		{
			//_displayOrbitPath->setSelected(orbitKernel->_orbitRendered);
		}
	}
}

void ObjectDetailsWindow::UpdateSystemInfo()
{
	EntityKernel * kernel = WindowManager::_mainApp->currentDetailsObject;
	if (kernel != NULL)
	{
		if (kernel->isCentralEntity())
		{
			OrbitingEntityKernel * orbitKernel = dynamic_cast<OrbitingEntityKernel *>(kernel);
			if (orbitKernel != NULL)
			{
				//_displayOrbits->setSelected(orbitKernel->_orbitsRendered);
			}

			SystemEntityKernel * parentKernel = kernel->GetParentKernel();
			std::string systemPosition = "System Position: NA";
			std::string systemNearestNeighbor = "System Position: NA";
			if (parentKernel != NULL)
			{
				Vector3 systemPos = parentKernel->GetPosition();
				std::string nearestNeighborText = GetNearestNeighborInfo(parentKernel);
				if (nearestNeighborText != "Not Available")
				{
					nearestNeighborText = "System Nearest Neighbor Name: " + nearestNeighborText;
				}
				systemPosition = "System Position (Parent System Relative): X-> " + std::to_string(systemPos[0]) + ", Y-> " + std::to_string(systemPos[1]) + ", Z-> " + std::to_string(systemPos[2]);
				systemNearestNeighbor = nearestNeighborText;
			}
			_systemPosition->setText(systemPosition);
			_systemNearestNeighbor->setText(systemNearestNeighbor);
		}
	}
}

void ObjectDetailsWindow::RenderOrbitingType(EntityKernel * kernel)
{
	OrbitingEntityKernel * orbitKernel = dynamic_cast<OrbitingEntityKernel *>(kernel);
	if (orbitKernel != NULL)
	{
		_orbitDistance->show();
		_orbitParentName->show();
		_orbitVelocity->show();
		_orbitParentType->show();
		_centralEntity->show();
		_displayOrbitPath->show();

		_displayOrbitPath->setSelected(orbitKernel->_orbitRendered);

		std::string parentName = "NA";
		std::string parentType = "NA";
		EntityKernel * orbitParent = orbitKernel->_orbitParent;

		if (orbitParent != NULL)
		{
			parentName = orbitParent->GetName();
			parentType = entitykernel::ConvertEntityTypeToString(orbitParent->GetType());
		}
		_orbitParentName->setText("Orbit Parent Name: " + parentName);

		_orbitParentType->setText("Orbit Parent Type: " + parentType);

		Vector3 orbitPos = orbitKernel->GetPosition();
		std::string isCentral = "No";
		if (orbitKernel->isCentralEntity())
		{
			isCentral = "Yes";
			BaseEntityKernel * parentKernel = orbitKernel->GetParentKernel();
			if (parentKernel != NULL)
			{
				orbitPos = parentKernel->GetPosition();
			}
		}
		_centralEntity->setText("Is System Central Object: " + isCentral);
		double distance = orbitPos.distance(Vector3(0, 0, 0));
		_orbitDistance->setText("Orbit Distance: " + std::to_string(distance));

		double angularSpeed = std::abs(orbitKernel->GetOrbitVelocity());
		double radiansPerGameYear = angularSpeed*EntityKernelManager::GetNumTicksPerGameYear();
		double revPerGameYear = radiansPerGameYear / (2 * M_PI);
		_orbitVelocity->setText("Revs Per Sim Year: " + std::to_string(revPerGameYear));
	}
}

void ObjectDetailsWindow::RenderCentralType(EntityKernel * kernel)
{
	if (kernel->isCentralEntity())
	{
		_numberOfOrbitingObjects->show();
		_displayOrbits->show();
		_systemNearestNeighbor->show();
		_systemPosition->show();

		OrbitingEntityKernel * orbitKernel = dynamic_cast<OrbitingEntityKernel*>(kernel);
		if (orbitKernel != NULL)
		{
			_displayOrbits->setSelected(orbitKernel->_orbitsRendered);
		}

		std::string systemPosition = "System Position: NA";
		std::string systemNearestNeighbor = "System Nearest Neighbor: NA";
		int numOrbitingObjects = EntityKernelManager::_systemMap.size() + EntityKernelManager::_loneEntityMap.size() - 1;

		SystemEntityKernel * parentKernel = kernel->GetParentKernel();
		if (parentKernel != NULL)
		{
			Vector3 systemPos = parentKernel->GetPosition();
			std::string nearestNeighborText = GetNearestNeighborInfo(parentKernel);
			if (nearestNeighborText != "Not Available")
			{
				nearestNeighborText = "System Nearest Neighbor Name: " + nearestNeighborText;
			}
			systemPosition = "System Position (Parent System Relative): X-> " + std::to_string(systemPos[0]) + ", Y-> " + std::to_string(systemPos[1]) + ", Z-> " + std::to_string(systemPos[2]);
			systemNearestNeighbor = nearestNeighborText;
			numOrbitingObjects = parentKernel->_systemMap.size() + parentKernel->_loneEntityMap.size() - 1;
		}
		_systemPosition->setText(systemPosition);
		_systemNearestNeighbor->setText(systemNearestNeighbor);
		_numberOfOrbitingObjects->setText("Num of System Orbiting Objects: " + std::to_string(numOrbitingObjects));
	}
}

void  ObjectDetailsWindow::HideAllNonGeneric()
{
	//orbit Type
	_orbitParentType->hide();
	_orbitParentName->hide();
	_orbitDistance->hide();
	_orbitVelocity->hide();
	_displayOrbitPath->hide();
	_centralEntity->hide();

	//Central Type
	_numberOfOrbitingObjects->hide();
	_displayOrbits->hide();
	_systemNearestNeighbor->hide();
	_systemPosition->hide();
}

MainMenu::MainMenu(CEGUI::Window * parent) : ChildWindow(parent)
{
	fullscreen = false;
	BaseApplication * mainApp = WindowManager::_mainApp;
	availableRes["1366,768"] = Vector2(1366, 768);
	curResKey = "1366,768";
	if (_parent != NULL && mainApp != NULL)
	{
		CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
		_thisWindow = wmgr.createWindow("TaharezLook/FrameWindow", "MenuFrame");
		CEGUI::FrameWindow * menuPane = dynamic_cast<CEGUI::FrameWindow*>(_thisWindow);
		if(menuPane != NULL)
		{
			CEGUI::Titlebar * titlebar = menuPane->getTitlebar();
			CEGUI::PushButton * closeButton = menuPane->getCloseButton();
			closeButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::mainMenuCancelPressed, mainApp));
			titlebar->setText("Main Menu");
			menuPane->setSize(CEGUI::USize(CEGUI::UDim(.75f, 0), CEGUI::UDim(0.75f, 0)));
			menuPane->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.25, 0)));
			parent->addChild(menuPane);
		}

		float curPos = 0;

		CEGUI::Window * comboLabel = wmgr.createWindow("TaharezLook/Label", "MainMenu/FullscreeComboLabel");
		comboLabel->setText("Fullscreen: ");
		comboLabel->setSize(CEGUI::USize(CEGUI::UDim(.25f, 0), CEGUI::UDim(0.1f, 0)));
		comboLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0, 0), CEGUI::UDim(curPos, 0)));
		_thisWindow->addChild(comboLabel);
		CEGUI::Window * comboBox = wmgr.createWindow("TaharezLook/Combobox", "MainMenu/FullscreeCombo");
		_fullscreen = static_cast<CEGUI::Combobox*>(comboBox);
		_fullscreen->setSize(CEGUI::USize(CEGUI::UDim(.25f, 0), CEGUI::UDim(0.2f, 0)));
		_fullscreen->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25, 0), CEGUI::UDim(curPos, 0)));
		_fullscreen->setReadOnly(true);
		CEGUI::ComboDropList * dropList = _fullscreen->getDropList();
		CEGUI::ListboxItem * yesItem = new CEGUI::ListboxTextItem("Yes");
		CEGUI::ListboxItem * noItem = new CEGUI::ListboxTextItem("No");
		dropList->addItem(yesItem);
		dropList->addItem(noItem);
		_fullscreen->setItemSelectState(noItem, true);
		_thisWindow->addChild(_fullscreen);

		curPos += 0.1;

		_generateButton = wmgr.createWindow("TaharezLook/Button", "MainMenu/Generate");
		_generateButton->setText("Generate Galaxy Options");
		_generateButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(curPos, 0)));
		_generateButton->setSize(CEGUI::USize(CEGUI::UDim(0.25f, 0.0f), CEGUI::UDim(0.1f, 0.0f)));
		_thisWindow->addChild(_generateButton);
		_generateButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::mainMenuGeneratePressed, mainApp));

		curPos += 0.2;

		_apply = wmgr.createWindow("TaharezLook/Button", "MainMenu/Apply");
		_apply->setText("Apply");
		_apply->setSize(CEGUI::USize(CEGUI::UDim(.25f, 0), CEGUI::UDim(0.1f, 0)));
		_apply->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0, 0), CEGUI::UDim(curPos, 0)));
		_thisWindow->addChild(_apply);
		_apply->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::mainMenuApplyPressed, mainApp));

		_cancel = wmgr.createWindow("TaharezLook/Button", "MainMenu/Cancel");
		_cancel->setText("Cancel");
		_cancel->setSize(CEGUI::USize(CEGUI::UDim(.25f, 0), CEGUI::UDim(0.1f, 0)));
		_cancel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25, 0), CEGUI::UDim(curPos, 0)));
		_thisWindow->addChild(_cancel);
		_cancel->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::mainMenuCancelPressed, mainApp));

		_quit = wmgr.createWindow("TaharezLook/Button", "MainMenu/QuitApp");
		_quit->setText("Quit Application");
		_quit->setSize(CEGUI::USize(CEGUI::UDim(.25f, 0), CEGUI::UDim(0.1f, 0)));
		_quit->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(0.9, 0)));
		_quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::quit, mainApp));
		_thisWindow->addChild(_quit);

		_thisWindow->hide();
	}
}

void MainMenu::CancelSettings()
{
	//reset full screen combo box
	if (fullscreen)
	{
		_fullscreen->setItemSelectState((size_t)0, true);
		_fullscreen->setItemSelectState((size_t)1, false);
	}
	else
	{
		_fullscreen->setItemSelectState((size_t)0, false);
		_fullscreen->setItemSelectState((size_t)1, true);
	}
}

void MainMenu::ApplySettings()
{
	//fullscreen setting apply
	CEGUI::ListboxItem * selection = _fullscreen->getSelectedItem();
	int index = _fullscreen->getItemIndex(selection);
	if (index == 0)
		fullscreen = true;
	else
		fullscreen = false;
}

void MainMenu::UpdateInfo()
{

}

void MainMenu::ResetInfo()
{
	_thisWindow->hide();
}

GenWindow::GenWindow(CEGUI::Window * parent) : ChildWindow(parent)
{
	BaseApplication * mainApp = WindowManager::_mainApp;
	if (_parent != NULL && mainApp != NULL)
	{
		CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
		_thisWindow = wmgr.createWindow("TaharezLook/FrameWindow", "GenerateFrame");
		CEGUI::FrameWindow * menuPane = dynamic_cast<CEGUI::FrameWindow*>(_thisWindow);
		if(menuPane != NULL)
		{
			CEGUI::Titlebar * titlebar = menuPane->getTitlebar();
			CEGUI::PushButton * closeButton = menuPane->getCloseButton();
			closeButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::generateMenuClosePressed, mainApp));
			titlebar->setText("Generate Galaxy");
			menuPane->setSize(CEGUI::USize(CEGUI::UDim(.75f, 0), CEGUI::UDim(0.75f, 0)));
			menuPane->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.25, 0)));
			parent->addChild(menuPane);
		}

		float curYPos = 0.0f;
		CEGUI::Window * starsLabel = wmgr.createWindow("TaharezLook/Label", "Generate/StarsLabel");
		starsLabel->setText("# Stars: ");
		starsLabel->setSize(CEGUI::USize(CEGUI::UDim(0.25f, 0), CEGUI::UDim(0.1f, 0)));
		starsLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curYPos, 0)));
		_thisWindow->addChild(starsLabel);
		CEGUI::Window * numStars = wmgr.createWindow("TaharezLook/Spinner", "Generate/Stars");
		_setNumStars = static_cast<CEGUI::Spinner*>(numStars);
		_setNumStars->setSize(CEGUI::USize(CEGUI::UDim(0.1f, 0), CEGUI::UDim(0.1f, 0)));
		_setNumStars->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25f, 0), CEGUI::UDim(curYPos, 0)));
		_setNumStars->setTextInputMode(CEGUI::Spinner::TextInputMode::Integer);
		_setNumStars->setStepSize(25);
		_setNumStars->setMinimumValue(25);
		_setNumStars->setMaximumValue(700);
		_setNumStars->setCurrentValue(50);
		_thisWindow->addChild(_setNumStars);

		curYPos += 0.1f;

		CEGUI::Window * recLabel = wmgr.createWindow("TaharezLook/Label", "Generate/RecLabel");
		recLabel->setText("Recursion Depth: ");
		recLabel->setSize(CEGUI::USize(CEGUI::UDim(0.25f, 0), CEGUI::UDim(0.1f, 0)));
		recLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curYPos, 0)));
		_thisWindow->addChild(recLabel);
		CEGUI::Window * recDepth = wmgr.createWindow("TaharezLook/Spinner", "Generate/Recursion");
		_setRecursionDepth = static_cast<CEGUI::Spinner*>(recDepth);
		_setRecursionDepth->setSize(CEGUI::USize(CEGUI::UDim(0.1f, 0), CEGUI::UDim(0.1f, 0)));
		_setRecursionDepth->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25f, 0), CEGUI::UDim(curYPos, 0)));
		_setRecursionDepth->setTextInputMode(CEGUI::Spinner::TextInputMode::Integer);
		_setRecursionDepth->setStepSize(1);
		_setRecursionDepth->setMinimumValue(0);
		_setRecursionDepth->setMaximumValue(2);
		_setRecursionDepth->setCurrentValue(1);
		_thisWindow->addChild(_setRecursionDepth);

		curYPos += 0.1f;

		CEGUI::Window * triesLabel = wmgr.createWindow("TaharezLook/Label", "Generate/TriesLabel");
		triesLabel->setText("Object Placement Tries: ");
		triesLabel->setSize(CEGUI::USize(CEGUI::UDim(0.25f, 0), CEGUI::UDim(0.1f, 0)));
		triesLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curYPos, 0)));
		_thisWindow->addChild(triesLabel);
		CEGUI::Window * tries = wmgr.createWindow("TaharezLook/Spinner", "Generate/Tries");
		_setNumPlacementTries = static_cast<CEGUI::Spinner*>(tries);
		_setNumPlacementTries->setSize(CEGUI::USize(CEGUI::UDim(0.1f, 0), CEGUI::UDim(0.1f, 0)));
		_setNumPlacementTries->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25f, 0), CEGUI::UDim(curYPos, 0)));
		_setNumPlacementTries->setTextInputMode(CEGUI::Spinner::TextInputMode::Integer);
		_setNumPlacementTries->setStepSize(10);
		_setNumPlacementTries->setMinimumValue(50);
		_setNumPlacementTries->setMaximumValue(2000);
		_setNumPlacementTries->setCurrentValue(500);
		_thisWindow->addChild(_setNumPlacementTries);

		curYPos += 0.1f;

		CEGUI::Window * sizeLabel = wmgr.createWindow("TaharezLook/Label", "Generate/SizeLabel");
		sizeLabel->setText("Galaxy Radius: ");
		sizeLabel->setSize(CEGUI::USize(CEGUI::UDim(0.25f, 0), CEGUI::UDim(0.1f, 0)));
		sizeLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(curYPos, 0)));
		_thisWindow->addChild(sizeLabel);
		CEGUI::Window * size = wmgr.createWindow("TaharezLook/Spinner", "Generate/Size");
		_setGalaxySize = static_cast<CEGUI::Spinner*>(size);
		_setGalaxySize->setSize(CEGUI::USize(CEGUI::UDim(0.1f, 0), CEGUI::UDim(0.1f, 0)));
		_setGalaxySize->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25f, 0), CEGUI::UDim(curYPos, 0)));
		_setGalaxySize->setTextInputMode(CEGUI::Spinner::TextInputMode::Integer);
		_setGalaxySize->setStepSize(2000);
		_setGalaxySize->setMinimumValue(2000);
		_setGalaxySize->setMaximumValue(2000);
		_setGalaxySize->setCurrentValue(2000);
		_thisWindow->addChild(_setGalaxySize);

		curYPos += 0.3f;

		_generateButton = wmgr.createWindow("TaharezLook/Button", "Generate/GenerateButton");
		_generateButton->setText("Generate Galaxy");
		_generateButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f), CEGUI::UDim(curYPos, 0)));
		_generateButton->setSize(CEGUI::USize(CEGUI::UDim(0.2f, 0.0f), CEGUI::UDim(0.1f, 0.0f)));
		_thisWindow->addChild(_generateButton);
		_generateButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::genMenuGeneratePressed, mainApp));

		_closeButton = wmgr.createWindow("TaharezLook/Button", "Generate/GenerateButton");
		_closeButton->setText("Close");
		_closeButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2f, 0.0f), CEGUI::UDim(curYPos, 0)));
		_closeButton->setSize(CEGUI::USize(CEGUI::UDim(0.2f, 0.0f), CEGUI::UDim(0.1f, 0.0f)));
		_thisWindow->addChild(_closeButton);
		_closeButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&BaseApplication::generateMenuClosePressed, mainApp));
		_thisWindow->hide();
	}
}

void GenWindow::UpdateInfo()
{

}

void GenWindow::ResetInfo()
{
	_thisWindow->hide();
}

void GenWindow::GeneratePressed()
{
	EntityKernelManager::SetNumSystems(_setNumStars->getCurrentValue());
	EntityKernelManager::_numGenerateTries = _setNumPlacementTries->getCurrentValue();
	EntityKernelManager::_recursionDepth = _setRecursionDepth->getCurrentValue();
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

BaseApplication * WindowManager::_mainApp = 0;
TopBarWindow * WindowManager::topBar = 0;
ObjectDetailsWindow * WindowManager::details = 0;
CEGUI::Window * WindowManager::_mainWindow = 0;
CEGUI::Window * WindowManager::_loadingWindow = 0;
CEGUI::Window * WindowManager::_loadingText = 0;
MainMenu * WindowManager::_menuWindow = 0;
GenWindow * WindowManager::_generateGalaxyOptions = 0;

void WindowManager::IntitializeWindows(BaseApplication * mainApplication)
{
	_mainApp = mainApplication;
	CEGUI::WindowManager &wmgr = CEGUI::WindowManager::getSingleton();
	CEGUI::Window * rootWindow = wmgr.createWindow("DefaultWindow", "ogreRootWindow");
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(rootWindow);
	_mainWindow = wmgr.createWindow("DefaultWindow", "ogreRootWindow/mainUI");
	_mainWindow->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(1.0f, 0)));
	rootWindow->addChild(_mainWindow);
	
	details = new ObjectDetailsWindow(_mainWindow);
	_menuWindow = new MainMenu(_mainWindow);
	_generateGalaxyOptions = new GenWindow(_mainWindow);
	_loadingWindow = wmgr.createWindow("TaharezLook/StaticImage", "LoadingBackground");
	_loadingWindow->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0), CEGUI::UDim(1.0f, 0)));
	rootWindow->addChild(_loadingWindow);
	_loadingWindow->hide();
	_loadingText = wmgr.createWindow("TaharezLook/Label", "LoadingBackground/Label");
	_loadingText->setText("Generating Galaxy");
	_loadingText->setPosition(CEGUI::UVector2(CEGUI::UDim(0.45f, 0.0f), CEGUI::UDim(0.4, 0)));
	_loadingText->setSize(CEGUI::USize(CEGUI::UDim(0.2f, 0.0f), CEGUI::UDim(0.2f, 0.0f)));
	_loadingWindow->addChild(_loadingText);
	
	HideDetails();
	InitTopBar();
}

WindowManager::~WindowManager()
{
	delete topBar;
	delete _generateGalaxyOptions;
	delete _menuWindow;
}

void WindowManager::UpdateInfo()
{
	topBar->UpdateInfo();
	details->UpdateInfo();
	_menuWindow->UpdateInfo();
	_generateGalaxyOptions->UpdateInfo();
}

void WindowManager::ResetInfo()
{
	topBar->ResetInfo();
	details->ResetInfo();
	_menuWindow->ResetInfo();
	_generateGalaxyOptions->ResetInfo();
}

void WindowManager::InitTopBar()
{
	topBar = new TopBarWindow(_mainWindow);
}

void WindowManager::ShowLoadingProgress()
{
	HideUI();
	_loadingWindow->show();
}

void WindowManager::HideLoadingProgress()
{
	_loadingWindow->hide();
	ShowUI();
}

void WindowManager::HideTopBar()
{
	if (topBar != NULL)
	{
		CEGUI::Window * menu = topBar->GetThisWindow();
		if (menu != NULL)
		{
			menu->hide();
		}
	}
}

void WindowManager::ShowTopBar()
{
	if (topBar != NULL)
	{
		CEGUI::Window * menu = topBar->GetThisWindow();
		if (menu != NULL)
		{
			menu->show();
		}
	}
}

void WindowManager::ShowMenu()
{
	if (_menuWindow != NULL)
	{
		CEGUI::Window * mainMenu = _menuWindow->GetThisWindow();
		if (mainMenu != NULL)
		{
			_mainApp->mCursorOn = true;
			CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
			_mainApp->_isMouseToggleable = false;
			mainMenu->show();
			_mainApp->mPauseSim = true;
		}
	}
}

void WindowManager::HideUI()
{
	_mainWindow->hide();
	HideGenerateGalaxyOptions();
	HideMenu();
}

void WindowManager::ShowUI()
{
	_mainWindow->show();
}

void WindowManager::HideMenu()
{
	if (_menuWindow != NULL)
	{
		CEGUI::Window * mainMenu = _menuWindow->GetThisWindow();
		if (mainMenu != NULL)
		{
			_mainApp->_isMouseToggleable = true;
			mainMenu->hide();
		}
	}
}

void WindowManager::ShowGenerateGalaxyOptions()
{
	if(_generateGalaxyOptions != NULL)
	{
		CEGUI::Window * gen = _generateGalaxyOptions->GetThisWindow();
		if(gen != NULL)
		{
			gen->show();
			gen->activate();
		}
	}
}

void WindowManager::HideGenerateGalaxyOptions()
{
	if(_generateGalaxyOptions != NULL)
	{
		CEGUI::Window * gen = _generateGalaxyOptions->GetThisWindow();
		if(gen != NULL)
		{
			_generateGalaxyOptions->GetThisWindow()->hide();
		}
	}
}

void WindowManager::SelectionChanged(EntityKernel * kernel)
{
	details->SelectionChanged(kernel);
}

void WindowManager::ShowDetails()
{
	if (details != NULL)
	{
		details->GetThisWindow()->show();
	}
}

void WindowManager::HideDetails()
{
	if (details != NULL)
	{
		details->GetThisWindow()->hide();
	}
}
