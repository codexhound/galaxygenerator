/*
-----------------------------------------------------------------------------
Filename:    BaseApplication.h
-----------------------------------------------------------------------------
Base Renderer application file
*/

#ifndef __BaseApplication_h_
#define __BaseApplication_h_

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreVector3.h>
#include <OgreSceneNode.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>

#include <OgreFrameListener.h>
#include <OgreMaterialManager.h>
#include <OgreTextureManager.h>
#include <OgreWindowEventUtilities.h>
#include <OgreTechnique.h>
#include <OgreMaterial.h>
#include <OgreRenderable.h>

#include "ThreadCommand.h"
class EntityKernel;

#ifdef OGRE_STATIC_LIB
#  define OGRE_STATIC_GL
#  if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#    define OGRE_STATIC_Direct3D9
// D3D10 will only work on vista, so be careful about statically linking
#    if OGRE_USE_D3D10
#      define OGRE_STATIC_Direct3D10
#    endif
#  endif
#  define OGRE_STATIC_BSPSceneManager
#  define OGRE_STATIC_ParticleFX
#  define OGRE_STATIC_CgProgramManager
#  ifdef OGRE_USE_PCZ
#    define OGRE_STATIC_PCZSceneManager
#    define OGRE_STATIC_OctreeZone
#  else
#    define OGRE_STATIC_OctreeSceneManager
#  endif
#  include "OgreStaticPluginLoader.h"
#endif

//---------------------------------------------------------------------------

namespace mainWindowApp
{
	enum MovementType
	{
		FORWARD,
		BACKWARD,
		LEFT,
		RIGHT,
		UP,
		DOWN
	};
}

class BaseApplication : public Ogre::FrameListener, public Ogre::WindowEventListener, public OIS::KeyListener, public OIS::MouseListener
{
	friend class WindowInitializer;
public:
	static CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
    BaseApplication(void);
    virtual ~BaseApplication(void);

    virtual void go(void);

	Ogre::SceneNode *mCameraNode;

	bool quit(const CEGUI::EventArgs &e);
	bool pause(const CEGUI::EventArgs &e);
	bool stoprenderorbits(const CEGUI::EventArgs &e);
	bool changesimspeed(const CEGUI::EventArgs &e);
	bool renderTrails(const CEGUI::EventArgs &e);
	bool renderPathChanged(const CEGUI::EventArgs &e);
	bool renderOrbitsChanged(const CEGUI::EventArgs &e);
	bool mainMenuApplyPressed(const CEGUI::EventArgs &e);
	bool mainMenuCancelPressed(const CEGUI::EventArgs &e);
	bool mainMenuGeneratePressed(const CEGUI::EventArgs &e);
	bool generateMenuClosePressed(const CEGUI::EventArgs & e);
	bool genMenuGeneratePressed(const CEGUI::EventArgs & e);

	std::list<ThreadCommand*> _threadCommandQueue;
	std::list<Ogre::Entity*> _selectionList;
	EntityKernel * currentDetailsObject;

	void ToggleMouseEntitySelection();
	Ogre::RaySceneQuery * raySceneQuery;

	void ProcessThreadQueue();

	Ogre::Real mSpeed;

	bool mPauseSim;
	bool _isMouseToggleable;
	bool first;
	bool newSim;
	bool postNewSim;

	void InitializeRendererForNewSim();
	void RenderNewSim();
	void PostRenderNewSim();
	void DestroySim();

	bool mCursorOn;

protected:
	void InitCameraNode();

	void LoadGUI();
	void ResetSettings()
	{
		mCursorOn = true;
		mPauseSim = true;
		mSpeed = 100.0;
	}
	void createInitialScene(void);
	void ProcessMovement(mainWindowApp::MovementType type);
    virtual bool setup();
    virtual bool configure(void);
    virtual void chooseSceneManager(void);
    virtual void createCamera(void);
    virtual void createFrameListener(void);
    virtual void destroyScene(void);
    virtual void createViewports(void);
    virtual void setupResources(void);
    virtual void createResourceListener(void);
    virtual void loadResources(void);
    virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

    virtual bool keyPressed(const OIS::KeyEvent &arg);
    virtual bool keyReleased(const OIS::KeyEvent &arg);
    virtual bool mouseMoved(const OIS::MouseEvent &arg);
    virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

    // Adjust mouse clipping area
    virtual void windowResized(Ogre::RenderWindow* rw);
    // Unattach OIS before window shutdown (very important under Linux)
    virtual void windowClosed(Ogre::RenderWindow* rw);

    Ogre::Root*                 mRoot;
    Ogre::Camera*               mCamera;
    Ogre::SceneManager*         mSceneMgr;
    Ogre::RenderWindow*         mWindow;
    Ogre::String                mResourcesCfg;
    Ogre::String                mPluginsCfg;

    bool                        mCursorWasVisible;	// Was cursor visible before dialog appeared?
    bool                        mShutDown;

    //OIS Input devices
    OIS::InputManager*          mInputManager;
    OIS::Mouse*                 mMouse;
    OIS::Keyboard*              mKeyboard;

    // Added for Mac compatibility
    Ogre::String                 m_ResourcePath;

	float mRotSpd;
	bool mLMouseDown, mRMouseDown, mMMouseDown;
	Ogre::Real cameraDistanceCenter;
	Ogre::Vector3 centerView;
	bool noZoomIn; //temporarily disallow zooming in

	//camera node
	CEGUI::Renderer* mRenderer;
	Ogre::Viewport * mViewport;
	Ogre::Real maxSpeed;
	Ogre::Real _timeSinceLastFrame;
	bool mWKeyDown;
	bool mSKeyDown;
	bool mDKeyDown;
	bool maKeyDown;
	bool mupKeyDown;
	bool mdownKeyDown;
	bool mEKeyDown;
	bool mQKeyDown;
	bool speedPosKeyDown;
	bool speedNegKeyDown;
	bool mLeftControlKeyDown;
	bool mSpaceKeyDown;
	bool mCKeyKeyDown;
	bool mOKeyKeyDown;
	

#ifdef OGRE_STATIC_LIB
    Ogre::StaticPluginLoader m_StaticPluginLoader;
#endif
};

//---------------------------------------------------------------------------

#endif // #ifndef __BaseApplication_h_

//---------------------------------------------------------------------------