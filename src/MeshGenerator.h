#pragma once
#include <OgreEntity.h>

using namespace Ogre;
class Entity;

namespace Verse
{
	class MeshGenerator
	{
	public:
		static Ogre::Entity * DonutEntity(std::string name, float radius, float thickness);
		static Ogre::Entity * ExtrudeCircleAlongPath(std::string name, float thickness, std::list<Vector3> &points);

	private:
		static float _accuracy;
		MeshGenerator() {}
	};
}

