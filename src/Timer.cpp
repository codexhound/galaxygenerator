#include "Timer.h"
using namespace Verse;

Timer::Timer(bool run)
{
	if (run)
		ResetTimer();
}

Timer::~Timer()
{
}

void Timer::ResetTimer()
{
	_start = high_resolution_clock::now();
}

milliseconds Timer::TimeElapsed()
{
	return std::chrono::duration_cast<milliseconds>(high_resolution_clock::now() - _start);
}

microseconds Timer::TimeElapsedMicroSeconds()
{
	return std::chrono::duration_cast<microseconds>(high_resolution_clock::now() - _start);
}
