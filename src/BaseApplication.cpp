
#include "BaseApplication.h"
#include <RTShaderSystem/OgreShaderExPerPixelLighting.h>
#include <OgreMaterialManager.h>
#include "EntityKernel.h"
#include "SystemEntityKernel.h"
#include "EntityKernelManager.h"
#include <fstream>
#include <iostream>
#include <random>
#include "CEGUI/widgets/All.h"
#include "WindowManager.h"
#include "OgreMeshManager.h"
#include "OgreParticleSystem.h"
#include "OgreParticle.h"
#include "CEGUI/widgets/ListboxItem.h"

#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#include <macUtils.h>
#endif

//---------------------------------------------------------------------------
BaseApplication::BaseApplication(void)
	: mRoot(0),
	mCamera(0),
	mSceneMgr(0),
	mWindow(0),
	mResourcesCfg(Ogre::StringUtil::BLANK),
	mPluginsCfg(Ogre::StringUtil::BLANK),
	mInputManager(0),
	mRotSpd(0.1),
	mKeyboard(0),
	mMouse(0),
	mShutDown(false),
	mLMouseDown(false),
	mRMouseDown(false),
	mMMouseDown(false),
	mCursorOn(true),
	cameraDistanceCenter(0.0),
	centerView(0.0, 0.0, 0.0),
	mSpeed(100.0),
	maxSpeed(2000.0),
	mCameraNode(0),
	noZoomIn(false),
	mWKeyDown(false),
	mSKeyDown(false),
	mDKeyDown(false),
	maKeyDown(false),
	mEKeyDown(false),
	mQKeyDown(false),
	_timeSinceLastFrame(0.0),
	mupKeyDown(false),
	speedPosKeyDown(false),
	speedNegKeyDown(false),
	mLeftControlKeyDown(false),
	mSpaceKeyDown(false),
	mdownKeyDown(false),
	mCKeyKeyDown(false),
	mPauseSim(true),
	mOKeyKeyDown(false),
	currentDetailsObject(NULL),
	raySceneQuery(NULL),
	first(true),
	newSim(false),
	postNewSim(false)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
    m_ResourcePath = Ogre::macBundlePath() + "/Contents/Resources/";
#else
    m_ResourcePath = "";
#endif
}

//---------------------------------------------------------------------------
BaseApplication::~BaseApplication(void)
{
    windowClosed(mWindow);
    delete mRoot;
}

bool BaseApplication::quit(const CEGUI::EventArgs &e)
{
	mShutDown = true;
    return true;
}

bool BaseApplication::pause(const CEGUI::EventArgs &e)
{
	mPauseSim = !mPauseSim;
	return true;
}

bool BaseApplication::stoprenderorbits(const CEGUI::EventArgs &e)
{
	//_threadCommandQueue.push_back(new StopRenderingAllOrbits());
	EntityKernelManager::StopRendereringAllOrbits();
	return true;
}

bool BaseApplication::renderTrails(const CEGUI::EventArgs &e)
{
	TopBarWindow * topBar = WindowManager::topBar;
	CEGUI::ToggleButton * toggleTrails = 0;
	if (topBar != NULL)
	{
		toggleTrails = topBar->_toggleTrails;
	}
	if (toggleTrails != NULL)
	{
		bool isChecked = toggleTrails->isSelected();
		if (isChecked)
		{
			EntityKernelManager::UpdateOrbitTrailRendering(true);
		}
		else
		{
			EntityKernelManager::UpdateOrbitTrailRendering(false);
		}
		return true;
	}
	return false;
}

bool BaseApplication::renderPathChanged(const CEGUI::EventArgs &e)
{
	//get state of sender
	ObjectDetailsWindow * detailsWindow = WindowManager::details;
	CEGUI::ToggleButton * renderPath = 0;
	if (detailsWindow != NULL)
	{
		renderPath = detailsWindow->_displayOrbitPath;
	}

	if (renderPath != NULL)
	{
		OrbitingEntityKernel * orbitKernel = dynamic_cast<OrbitingEntityKernel*>(currentDetailsObject);
		if (orbitKernel != NULL)
		{
			//OrbitPathRender * orbitRenderCommand = new OrbitPathRender();
			//orbitRenderCommand->entity = orbitKernel;
			//if (renderPath->isSelected() && !orbitKernel->_orbitRendered)
			if (renderPath->isSelected())
			{
				//orbitRenderCommand->render = true;
				EntityKernelManager::RenderOrbitPath(orbitKernel);
			}
			//else if(!renderPath->isSelected())
			else
			{
				//orbitRenderCommand->render = false;
				//renderPath->setSelected(false);
				EntityKernelManager::StopRenderOrbitPath(orbitKernel);
			}
			//_threadCommandQueue.push_back(orbitRenderCommand);
			return true;
		}
	}
	return false;
}

bool BaseApplication::mainMenuApplyPressed(const CEGUI::EventArgs &e)
{
	MainMenu *mainWindow = WindowManager::_menuWindow;
	CEGUI::Combobox * fullscreen = 0;
	if (mainWindow != NULL)
	{
		mainWindow->ApplySettings();
		Vector2 curRes = mainWindow->availableRes[mainWindow->curResKey];
		if (mainWindow->fullscreen)
		{
			mWindow->setFullscreen(true, curRes[0], curRes[1]);
		}
		else
		{
			mWindow->setFullscreen(false, curRes[0], curRes[1]);
		}
		CEGUI::Sizef size(curRes[0], curRes[1]);
		mRenderer->setDisplaySize(size);
		return true;
	}
	return false;
}

bool BaseApplication::mainMenuCancelPressed(const CEGUI::EventArgs &e)
{
	MainMenu *mainWindow = WindowManager::_menuWindow;
	CEGUI::Combobox * fullscreen = 0;
	if (mainWindow != NULL)
	{
		mainWindow->CancelSettings();
		WindowManager::HideMenu();
		return true;
	}
	return false;
}

bool BaseApplication::mainMenuGeneratePressed(const CEGUI::EventArgs &e)
{
	WindowManager::ShowGenerateGalaxyOptions();
	return true;
}

bool BaseApplication::generateMenuClosePressed(const CEGUI::EventArgs & e)
{
	WindowManager::HideGenerateGalaxyOptions();
	return true;
}

bool BaseApplication::genMenuGeneratePressed(const CEGUI::EventArgs & e)
{
	GenWindow * gen = WindowManager::_generateGalaxyOptions;
	if (gen != NULL)
	{
		gen->GeneratePressed();
		WindowManager::_mainApp->InitializeRendererForNewSim();
		return true;
	}
	return false;
}

bool BaseApplication::renderOrbitsChanged(const CEGUI::EventArgs &e)
{
	//get state of sender
	ObjectDetailsWindow * detailsWindow = WindowManager::details;
	CEGUI::ToggleButton * renderOrbits = 0;
	if (detailsWindow != NULL)
	{
		renderOrbits = detailsWindow->_displayOrbits;
	}

	if (renderOrbits != NULL)
	{
		OrbitingEntityKernel * orbitKernel = dynamic_cast<OrbitingEntityKernel*>(currentDetailsObject);
		if (orbitKernel != NULL)
		{
			//OrbitRender * orbitRenderCommand = new OrbitRender();
			//orbitRenderCommand->centralEntity = orbitKernel;
			//if (renderOrbits->isSelected() && !orbitKernel->_orbitsRendered)
			if (renderOrbits->isSelected())
			{
				//orbitRenderCommand->render = true;
				EntityKernelManager::RenderOrbits(orbitKernel);
			}
			//else if(!renderOrbits->isSelected())
			else
			{
				//renderOrbits->setSelected(false);
				//orbitRenderCommand->render = false;
				EntityKernelManager::StopRenderingOrbits(orbitKernel);
			}
			//_threadCommandQueue.push_back(orbitRenderCommand);
			return true;
		}
	}
	return false;
}

bool BaseApplication::changesimspeed(const CEGUI::EventArgs &e)
{
	const CEGUI::WindowEventArgs& wea = static_cast<const CEGUI::WindowEventArgs&>(e);
	TopBarWindow * topBar = WindowManager::topBar;
	CEGUI::Window * increaseSpeed = 0;
	if (topBar != NULL)
	{
		increaseSpeed = topBar->_increaseSpeed;
	}

	if (wea.window == increaseSpeed)
	{
		if (EntityKernelManager::_gameSpeedFactor < 10)
		{
			if (EntityKernelManager::_gameSpeedFactor < 0.99)
			{
				EntityKernelManager::_gameSpeedFactor += 0.1;
			}
			else
			{
				++EntityKernelManager::_gameSpeedFactor;
			}
		}
		else EntityKernelManager::_gameSpeedFactor = 10;
	}
	else
	{
		if (EntityKernelManager::_gameSpeedFactor > 0.09)
		{
			if (EntityKernelManager::_gameSpeedFactor > 1)
			{
				--EntityKernelManager::_gameSpeedFactor;
			}
			else
			{
				EntityKernelManager::_gameSpeedFactor -= 0.1;
			}
		}
		else EntityKernelManager::_gameSpeedFactor = 0;
	}
	return true;
}

//---------------------------------------------------------------------------
bool BaseApplication::configure(void)
{
    // Show the configuration dialog and initialise the system.
    // You can skip this and use root.restoreConfig() to load configuration
    // settings if you were sure there are valid ones saved in ogre.cfg.
    //if(mRoot->showConfigDialog())
	if(mRoot->restoreConfig())
    {
        // If returned true, user clicked OK so initialise.
        // Here we choose to let the system create a default rendering window by passing 'true'.
		mWindow = mRoot->initialise(true, "3D Galaxy Generator");

        return true;
    }
    else
    {
        return false;
    }
}

void BaseApplication::InitCameraNode()
{
	Ogre::SceneNode * node = mSceneMgr->getRootSceneNode();
	mCameraNode = node->createChildSceneNode("Camera Node");
	mCameraNode->setPosition(Ogre::Vector3(0, 0, EntityKernelManager::GetGalaxyRadius() *0.5));
	mCameraNode->attachObject(mCamera);

	cameraDistanceCenter = EntityKernelManager::GetGalaxyRadius() * 3;
	// Look back along -Z
	mCamera->lookAt(Ogre::Vector3(0, 0, -300));
	mCamera->setNearClipDistance(0.001);
}

void BaseApplication::createInitialScene(void)
{
	mSceneMgr->setAmbientLight(Ogre::ColourValue(1.0, 1.0, 1.0));

	Ogre::Light* light = mSceneMgr->createLight("MainLight");
	light->setType(Light::LT_POINT);
	light->setPosition(0, 0, 0);

	mSceneMgr->setSkyBox(true, "MySky", 5000);
}

void BaseApplication::LoadGUI()
{
	mRenderer = &CEGUI::OgreRenderer::bootstrapSystem();

	/*Load the GUI*/

	CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
	CEGUI::Font::setDefaultResourceGroup("Fonts");
	CEGUI::Scheme::setDefaultResourceGroup("Schemes");
	CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

	CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

	WindowManager::IntitializeWindows(this); //initialize CEGUI system
}

CEGUI::MouseButton BaseApplication::convertButton(OIS::MouseButtonID buttonID)
{
	switch (buttonID)
	{
	case OIS::MB_Left:
		return CEGUI::LeftButton;

	case OIS::MB_Right:
		return CEGUI::RightButton;

	case OIS::MB_Middle:
		return CEGUI::MiddleButton;

	default:
		return CEGUI::LeftButton;
    }
}
//---------------------------------------------------------------------------
void BaseApplication::chooseSceneManager(void)
{
    // Get the SceneManager, in this case a generic one
    mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);
	raySceneQuery = mSceneMgr->createRayQuery(Ogre::Ray());
	AxisAlignedBox box;
	Vector3 max(100000, 100000, 100000);
	box.setExtents(-max, max);
	mSceneMgr->setOption("Size", &box);

}
//---------------------------------------------------------------------------
void BaseApplication::createCamera(void)
{
    // Create the camera
	if(mCamera == NULL)
		mCamera = mSceneMgr->createCamera("ThirdView");
}
//---------------------------------------------------------------------------
void BaseApplication::createFrameListener(void)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    mInputManager = OIS::InputManager::createInputSystem(pl);

    mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));
    mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));

    mMouse->setEventCallback(this);
    mKeyboard->setEventCallback(this);

    // Set initial mouse clipping size
    windowResized(mWindow);

    mRoot->addFrameListener(this);
}
//---------------------------------------------------------------------------
void BaseApplication::destroyScene(void)
{
	mSceneMgr->clearScene();
	mSceneMgr->destroyAllCameras();
}
//---------------------------------------------------------------------------
void BaseApplication::createViewports(void)
{
    // Create one viewport, entire window
	mViewport = mWindow->addViewport(mCamera);
	mViewport->setBackgroundColour(Ogre::ColourValue(0.1,0.1,0.2));

    // Alter the camera aspect ratio to match the viewport
    mCamera->setAspectRatio(Ogre::Real(mViewport->getActualWidth()) / Ogre::Real(mViewport->getActualHeight()));
}

//---------------------------------------------------------------------------
bool BaseApplication::setup(void)
{
	mRoot = new Ogre::Root(mPluginsCfg);

	setupResources();

	bool carryOn = configure();
	if (!carryOn) return false;

	// Set default mipmap level (NB some APIs ignore this)
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

	// Create any resource listeners (for loading screens)
	createResourceListener();
	// Load resources
	loadResources();

	chooseSceneManager();

	EntityKernelManager::Inititalize(mSceneMgr, this);

	createCamera();

	createViewports();

	LoadGUI();

	EntityKernelManager::InitializeParticleSystems();
	
	createFrameListener();

	WindowManager::ShowMenu();

	return true;
};

//---------------------------------------------------------------------------
void BaseApplication::setupResources(void)
{
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;

    #if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
                // OS X does not set the working directory relative to the app.
                // In order to make things portable on OS X we need to provide
                // the loading with it's own bundle path location.
                if (!Ogre::StringUtil::startsWith(archName, "/", false)) // only adjust relative directories
                    archName = Ogre::String(Ogre::macBundlePath() + "/" + archName);
    #endif

            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
                archName, typeName, secName);
        }
    }
}
//---------------------------------------------------------------------------
void BaseApplication::createResourceListener(void)
{
}
//---------------------------------------------------------------------------
void BaseApplication::loadResources(void)
{
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
//---------------------------------------------------------------------------
void BaseApplication::go(void)
{
    #ifdef _DEBUG
    #ifndef OGRE_STATIC_LIB
        mResourcesCfg = m_ResourcePath + "resources_d.cfg";
        mPluginsCfg = m_ResourcePath + "plugins_d.cfg";
    #else
        mResourcesCfg = "resources_d.cfg";
        mPluginsCfg = "plugins_d.cfg";
    #endif
    #else
    #ifndef OGRE_STATIC_LIB
        mResourcesCfg = m_ResourcePath + "resources.cfg";
        mPluginsCfg = m_ResourcePath + "plugins.cfg";
    #else
        mResourcesCfg = "resources.cfg";
        mPluginsCfg = "plugins.cfg";
    #endif
    #endif

    if (!setup())
        return;

    mRoot->startRendering();

    // Clean up
    destroyScene();
}

void BaseApplication::InitializeRendererForNewSim()
{
	newSim = true;

	WindowManager::ShowLoadingProgress();
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
	WindowManager::ResetInfo();
	ResetSettings(); //reset the app settings to default
	_isMouseToggleable = false;
}

void BaseApplication::DestroySim()
{
	mSceneMgr->clearScene(); //remove all nodes, entities, and lights
	Ogre::MeshManager::getSingleton().removeAll(); //remove the meshes
	EntityKernelManager::RemoveMaterials(); //remove the materials and textures
	EntityKernelManager::RemoveGalaxy(); //delete the galaxy	
}

void BaseApplication::RenderNewSim()
{
	newSim = false;
	postNewSim = true;

	DestroySim();
	EntityKernelManager::GenerateGalaxy(); //create the galaxy
	EntityKernelManager::UpdateEntitiesForTimeStep(); //calculate the galaxies next step
	InitCameraNode();
	createInitialScene(); //recreate lights/skybox
}

void BaseApplication::PostRenderNewSim()
{
	postNewSim = false;

	_isMouseToggleable = true;
	CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
	WindowManager::HideLoadingProgress();
}

//---------------------------------------------------------------------------
bool BaseApplication::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	if (first)
	{
		first = false;
		//InitializeRendererForNewSim();
	}
	else if (newSim)
	{
		RenderNewSim();
	}
	else if (postNewSim)
	{
		PostRenderNewSim();
		//DestroySim();
	}
	
	int numMicroSecondsElapsed = EntityKernelManager::_timer.TimeElapsedMicroSeconds().count();
	WindowManager::UpdateInfo();

	if (EntityKernelManager::CheckThreadsIfReady())
	{
		ProcessThreadQueue();
		if (numMicroSecondsElapsed > EntityKernelManager::GetNumMicroSecondsPerTick() && !mPauseSim && EntityKernelManager::_gameSpeedFactor > 0.09 && EntityKernelManager::DoesGalaxyExist())
		{
			EntityKernelManager::UpdateGameYear();

			EntityKernelManager::ClearThreadList(); //clear the previous thread list
														//update from all system thread outputs
			EntityKernelManager::UpdateFromSystemThreadOutputs();

			//Then restart the threads for next tick
			EntityKernelManager::UpdateEntitiesForTimeStep();
		}
	}

    if(mWindow->isClosed())
        return false;

    if(mShutDown)
        return false;


    // Need to capture/update each device
    mKeyboard->capture();
    mMouse->capture();

	_timeSinceLastFrame = evt.timeSinceLastEvent;

	if (mWKeyDown)
		ProcessMovement(mainWindowApp::FORWARD);
	else if (mSKeyDown)
		ProcessMovement(mainWindowApp::BACKWARD);
	if (mDKeyDown)
		ProcessMovement(mainWindowApp::RIGHT);
	else if (maKeyDown)
		ProcessMovement(mainWindowApp::LEFT);
	if (mupKeyDown || mSpaceKeyDown)
		ProcessMovement(mainWindowApp::UP);
	else if (mdownKeyDown || mLeftControlKeyDown || mCKeyKeyDown)
		ProcessMovement(mainWindowApp::DOWN);
	if (mEKeyDown)
		mCameraNode->roll(Ogre::Degree(-1));
	else if (mQKeyDown)
		mCameraNode->roll(Ogre::Degree(1));

	if (mOKeyKeyDown)
	{

	}

	if (speedPosKeyDown && mSpeed < maxSpeed)
	{
		mSpeed = mSpeed + _timeSinceLastFrame*mSpeed*0.5 + 0.1;
	}
	else if (speedNegKeyDown && mSpeed > 0)
	{
		mSpeed = mSpeed - _timeSinceLastFrame*mSpeed*0.5 - 0.1;
	}

	if (mSpeed < 0)
	{
		mSpeed = 0;
	}
	if (mSpeed > maxSpeed)
	{
		mSpeed = maxSpeed;
	}

	//Need to inject timestamps to CEGUI System.
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);

    return true;
}

void BaseApplication::ProcessMovement(mainWindowApp::MovementType type)
{
	double moveAmount = _timeSinceLastFrame * mSpeed;
	switch (type)
	{
	case mainWindowApp::FORWARD:
		mCameraNode->translate(Ogre::Vector3(0, 0, -moveAmount), Ogre::Node::TS_LOCAL);
		break;
	case mainWindowApp::BACKWARD:
		mCameraNode->translate(Ogre::Vector3(0, 0, moveAmount), Ogre::Node::TS_LOCAL);
		break;
	case mainWindowApp::RIGHT:
		mCameraNode->translate(Ogre::Vector3(moveAmount, 0, 0), Ogre::Node::TS_LOCAL);
		break;
	case mainWindowApp::LEFT:
		mCameraNode->translate(Ogre::Vector3(-moveAmount, 0, 0), Ogre::Node::TS_LOCAL);
		break;
	case mainWindowApp::UP:
		mCameraNode->translate(Ogre::Vector3(0, moveAmount, 0), Ogre::Node::TS_LOCAL);
		break;
	case mainWindowApp::DOWN:
		mCameraNode->translate(Ogre::Vector3(0, -moveAmount, 0), Ogre::Node::TS_LOCAL);
		break;
	default:
		break;
	}
}

//---------------------------------------------------------------------------
bool BaseApplication::keyPressed( const OIS::KeyEvent &arg )
{
	switch (arg.key)
	{
		case OIS::KC_ESCAPE:
			WindowManager::ShowMenu();
			break;
		case OIS::KC_W:
			mWKeyDown = true;
			break;
		case OIS::KC_S:
			mSKeyDown = true;
			break;
		case OIS::KC_D:
			mDKeyDown = true;
			break;
		case OIS::KC_RIGHT:
			mDKeyDown = true;
			break;
		case OIS::KC_A:
			maKeyDown = true;
			break;
		case OIS::KC_LEFT:
			maKeyDown = true;
			break;
		case OIS::KC_UP:
			mupKeyDown = true;
			break;
		case OIS::KC_DOWN:
			mdownKeyDown = true;
			break;
		case OIS::KC_E:
			mEKeyDown = true;
			break;
		case OIS::KC_Q:
			mQKeyDown = true;
			break;
		case OIS::KC_PERIOD:
			speedPosKeyDown = true;
			break;
		case OIS::KC_COMMA:
			speedNegKeyDown = true;
			break;
		case OIS::KC_SPACE:
			mSpaceKeyDown = true;
			break;
		case OIS::KC_LCONTROL:
			mLeftControlKeyDown = true;
			break;
		case OIS::KC_C:
			mCKeyKeyDown = true;
			break;
		case OIS::KC_O:
			mOKeyKeyDown = false;
			break;
		default:
			break;
	}

	CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
	context.injectKeyDown((CEGUI::Key::Scan)arg.key);
	context.injectChar((CEGUI::Key::Scan)arg.text);

    return true;
}
//---------------------------------------------------------------------------
bool BaseApplication::keyReleased(const OIS::KeyEvent &arg)
{
	switch (arg.key)
	{
	case OIS::KC_W:
		mWKeyDown = false;
		break;
	case OIS::KC_S:
		mSKeyDown = false;
		break;
	case OIS::KC_D:
		mDKeyDown = false;
		break;
	case OIS::KC_RIGHT:
		mDKeyDown = false;
		break;
	case OIS::KC_A:
		maKeyDown = false;
		break;
	case OIS::KC_LEFT:
		maKeyDown = false;
		break;
	case OIS::KC_UP:
		mupKeyDown = false;
		break;
	case OIS::KC_DOWN:
		mdownKeyDown = false;
		break;
	case OIS::KC_E:
		mEKeyDown = false;
		break;
	case OIS::KC_Q:
		mQKeyDown = false;
		break;
	case OIS::KC_PERIOD:
		speedPosKeyDown = false;
		break;
	case OIS::KC_COMMA:
		speedNegKeyDown = false;
		break;
	case OIS::KC_SPACE:
		mSpaceKeyDown = false;
		break;
	case OIS::KC_LCONTROL:
		mLeftControlKeyDown = false;
		break;
	case OIS::KC_C:
		mCKeyKeyDown = false;
		break;
	case OIS::KC_O:
		mOKeyKeyDown = true;
		break;
	default:
		break;
	}

	CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp((CEGUI::Key::Scan)arg.key);
    return true;
}

//---------------------------------------------------------------------------
bool BaseApplication::mouseMoved(const OIS::MouseEvent &arg)
{
	if (!mCursorOn)
	{
		mCameraNode->yaw(Ogre::Degree(-arg.state.X.rel * mRotSpd));
		mCameraNode->pitch(Ogre::Degree(-arg.state.Y.rel * mRotSpd));
	}

	CEGUI::System &sys = CEGUI::System::getSingleton();
	sys.getDefaultGUIContext().injectMouseMove(arg.state.X.rel, arg.state.Y.rel);
	// Scroll wheel.
	if (arg.state.Z.rel)
		sys.getDefaultGUIContext().injectMouseWheelChange(arg.state.Z.rel / 120.0f);

    return true;
}
//---------------------------------------------------------------------------
bool BaseApplication::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
	float xPos = (float)arg.state.X.abs / (float)mViewport->getActualWidth();
	float yPos = (float)arg.state.Y.abs / (float)mViewport->getActualHeight();
	if (_isMouseToggleable)
	{
		if (id == OIS::MB_Left)
		{
			if (!mLMouseDown && ((xPos > 0.3 || (xPos < 0.3 && yPos < 0.6)) || currentDetailsObject == NULL))
			{
				ToggleMouseEntitySelection();
			}
			mLMouseDown = true;
		}
		else if (id == OIS::MB_Right)
		{
			mRMouseDown = true;
		}
		else if (id == OIS::MB_Middle && !mMMouseDown)
		{
			if (!mCursorOn)
			{
				CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
				mCursorOn = true;
			}
			else
			{
				CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
				mCursorOn = false;
			}
			mMMouseDown = true;
		}
	}
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertButton(id));

    return true;
}
//---------------------------------------------------------------------------
bool BaseApplication::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertButton(id));
	if (id == OIS::MB_Left)
	{
		mLMouseDown = false;
	}
	else if (id == OIS::MB_Right)
	{
		mRMouseDown = false;
	}
	else if(id == OIS::MB_Middle)
	{
		mMMouseDown = false;
	}

    return true;
}
//---------------------------------------------------------------------------
// Adjust mouse clipping area
void BaseApplication::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width = width;
    ms.height = height;

}
//---------------------------------------------------------------------------
// Unattach OIS before window shutdown (very important under Linux)
void BaseApplication::windowClosed(Ogre::RenderWindow* rw)
{
    // Only close for window that created OIS (the main window in these demos)
    if(rw == mWindow)
    {
        if(mInputManager)
        {
            mInputManager->destroyInputObject(mMouse);
            mInputManager->destroyInputObject(mKeyboard);

            OIS::InputManager::destroyInputSystem(mInputManager);
            mInputManager = 0;
        }
    }
}

void BaseApplication::ProcessThreadQueue()
{
	for (auto commandIt = _threadCommandQueue.begin(); commandIt != _threadCommandQueue.end(); commandIt++)
	{
		(*commandIt)->Process();
		delete (*commandIt);
	}
	_threadCommandQueue.clear();
}

void BaseApplication::ToggleMouseEntitySelection()
{
	CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();


	float xPos = (float)mousePos.d_x / (float)mViewport->getActualWidth();
	float yPos = (float)mousePos.d_y / (float)mViewport->getActualHeight();
	Ogre::Ray mouseRay =
		mCamera->getCameraToViewportRay(xPos, yPos);

	raySceneQuery->setRay(mouseRay);
	raySceneQuery->setSortByDistance(true);

	Ogre::RaySceneQueryResult& result = raySceneQuery->execute();

	EntityKernel * kernel = 0;
	Ogre::MovableObject * curEntity = 0;

	for (auto it = result.begin(); it != result.end(); it++)
	{
		curEntity = it->movable;
		if (curEntity != NULL)
		{
			Ogre::Node * parentNode = curEntity->getParentNode();
			if (parentNode != NULL)
			{
				Ogre::Any anyKernel = parentNode->getUserAny();
				if (!anyKernel.isEmpty())
				{
					kernel = any_cast<OrbitingEntityKernel*>(anyKernel);
					if (kernel != NULL)
					{
						break; //intersection kernel found
					}
				}
			}
		}
	}

	if (kernel == NULL) //no object was found
	{
		if (currentDetailsObject != NULL)
		{
			currentDetailsObject->isSelected = false; //deselect current entity
			currentDetailsObject->_selectionSystem->setVisible(currentDetailsObject->isSelected);
		}
		currentDetailsObject = NULL; //set current selected entity to NULL
	}
	else //entity was found
	{
		if (kernel->_selectionSystem->getNumParticles() == 0)
		{
			Particle * particle = kernel->_selectionSystem->createParticle();
			particle->mParticleType = Particle::ParticleType::Visual;
			particle->mColour = ColourValue(ColourValue::White);
			particle->setDimensions((Real)kernel->GetRadius() * 3, (Real)kernel->GetRadius() * 3);
		}

		if (currentDetailsObject != NULL)
		{
			currentDetailsObject->isSelected = !currentDetailsObject->isSelected;
			currentDetailsObject->_selectionSystem->setVisible(currentDetailsObject->isSelected);
		}

		if (currentDetailsObject != kernel)
		{
			kernel->isSelected = true;
			currentDetailsObject = kernel;
			currentDetailsObject->_selectionSystem->setVisible(true);
		}
		else //deselect
		{
			currentDetailsObject = NULL;
		}
	}
	WindowManager::SelectionChanged(currentDetailsObject);
}

//---------------------------------------------------------------------------
