#include "GalaxyTextureGen.h"
#include "EntityKernelManager.h"
#include "Procedural.h"
#include <iostream>

using namespace Verse;

int GalaxyTextureGen::_amountofMaterialsToGenerateEach = 200;
std::vector<Ogre::MaterialPtr> GalaxyTextureGen::_planetMaterialList;
std::vector<Ogre::MaterialPtr> GalaxyTextureGen::_starMaterialList;

Ogre::MaterialPtr GalaxyTextureGen::GeneratePlanetMaterial(std::string name)
{
	std::uniform_real_distribution<float> rgbDist(0, 1);

	// Basic structure
	Procedural::TextureBuffer distort(256);
	float red = rgbDist(EntityKernelManager::_generator);
	float green = rgbDist(EntityKernelManager::_generator);
	float blue = rgbDist(EntityKernelManager::_generator);
	Ogre::ColourValue colorValue(red, green, blue,1.0);
	Procedural::Solid(&distort).setColour(colorValue).process();

	std::string uniqueNameTex = "procedural_" + name + "_Texture";
	std::string uniqueNameMaterial = "procedural_" + name + "_Material";
	Ogre::TexturePtr texture = distort.createTexture(uniqueNameTex);

	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingletonPtr()->create(uniqueNameMaterial, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	material->getTechnique(0)->getPass(0)->setShininess(50);
	material->getTechnique(0)->getPass(0)->setDiffuse(Ogre::ColourValue::White);
	material->getTechnique(0)->getPass(0)->setSpecular(Ogre::ColourValue(1.0f, 1.0f, 0.9f));
	material->getTechnique(0)->getPass(0)->createTextureUnitState(uniqueNameTex);
	return material;
}

Ogre::MaterialPtr GalaxyTextureGen::GenerateStarMaterial(std::string name)
{
	std::uniform_real_distribution<float> rgbDist(0.5, 1);

	// Basic structure
	Procedural::TextureBuffer base(256);

	float red = 1.0;
	float green = rgbDist(EntityKernelManager::_generator);
	float blue = 1.0;
	Ogre::ColourValue colorValue(red, green, blue, 0.2);

	Procedural::Solid(&base).setColour(colorValue).process();

	std::string uniqueNameTex = "procedural_" + name + "_Texture";
	std::string uniqueNameMaterial = "procedural_" + name + "_Material";
	Ogre::TexturePtr texture = base.createTexture(uniqueNameTex);

	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingletonPtr()->create(uniqueNameMaterial, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	
	//material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
	//material->getTechnique(0)->getPass(0)->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
	material->getTechnique(0)->getPass(0)->setShininess(30);
	material->getTechnique(0)->getPass(0)->setDiffuse(Ogre::ColourValue::White);
	material->getTechnique(0)->getPass(0)->setSpecular(Ogre::ColourValue(1.0f, 1.0f, 0.9f));
	material->getTechnique(0)->getPass(0)->setEmissive(Ogre::ColourValue::White);
	material->getTechnique(0)->getPass(0)->createTextureUnitState(uniqueNameTex);
	return material;
}

Ogre::MaterialPtr GalaxyTextureGen::GenerateBlackHoleMaterial(std::string name)
{
	Ogre::ColourValue colorValue(0.05f, 0.05f, 0.1f, 0.5f);
	// Basic structure
	Procedural::TextureBuffer base(256);

	Procedural::Solid(&base).setColour(colorValue).process();

	std::string uniqueNameTex = "procedural_" + name + "_Texture";
	std::string uniqueNameMaterial = "procedural_" + name + "_Material";
	Ogre::TexturePtr texture = base.createTexture(uniqueNameTex);

	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingletonPtr()->create(uniqueNameMaterial, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	material->getTechnique(0)->getPass(0)->setShininess(30);
	material->getTechnique(0)->getPass(0)->setDiffuse(Ogre::ColourValue::White);
	material->getTechnique(0)->getPass(0)->setSpecular(Ogre::ColourValue(1.0f, 1.0f, 0.9f));
	material->getTechnique(0)->getPass(0)->setEmissive(Ogre::ColourValue::White);
	material->getTechnique(0)->getPass(0)->createTextureUnitState(uniqueNameTex);
	return material;
}

void GalaxyTextureGen::GenerateMaterials()
{
	std::string name = "";
	ResetMaterials();
	for (int i = 0; i < _amountofMaterialsToGenerateEach; i++)
	{
		name = "_a" + std::to_string(i);
		_planetMaterialList.push_back(GeneratePlanetMaterial(name+"Planet"));
		_starMaterialList.push_back(GenerateStarMaterial(name+"Star"));
	}
}

void GalaxyTextureGen::ResetMaterials()
{
	_planetMaterialList.clear();
	_starMaterialList.clear();
}

Ogre::MaterialPtr GalaxyTextureGen::GetRandomMaterial(EntityType type)
{
	Ogre::MaterialPtr material;
	if (MaterialsExist())
	{
		if (type == STAR)
		{
			std::uniform_int_distribution<int> dist(0, (int)(_starMaterialList.size() - 1));
			material = _starMaterialList.at(dist(EntityKernelManager::_generator));
		}
		else if (type == PLANET || type == MOON)
		{
			std::uniform_int_distribution<int> dist(0, (int)(_planetMaterialList.size() - 1));
			material = _planetMaterialList.at(dist(EntityKernelManager::_generator));
		}
	}
	
	return material;
}

bool GalaxyTextureGen::MaterialsExist()
{
	bool exist = false;
	if (_starMaterialList.size() > 0 && _planetMaterialList.size() > 0)
	{
		exist = true;
	}

	return exist;
}
	