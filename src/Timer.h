#pragma once
#include <chrono>

namespace Verse
{
	typedef std::chrono::high_resolution_clock high_resolution_clock;
	typedef std::chrono::milliseconds milliseconds;
	typedef std::chrono::microseconds microseconds;

	class Timer
	{
	public:
		explicit Timer(bool run = false);
		~Timer();

		void ResetTimer();
		milliseconds TimeElapsed();
		microseconds TimeElapsedMicroSeconds();

		template <typename T, typename Traits>
		friend std::basic_ostream<T, Traits>& operator<<(std::basic_ostream<T, Traits>& out, const Timer& timer)
		{
			return out << timer.TimeElapsed().count();
		}

	private:
		high_resolution_clock::time_point _start;
	};
}

