#include "EntityKernel.h"
#include "EntityKernelManager.h"
#include "SystemEntityKernel.h"
#include <iostream>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include "Procedural.h"
#include "GalaxyTextureGen.h"
#include "MathDefines.h"
#include "OgreEllipsoidEmitterFactory.h"
#include "OgreLinearForceAffectorFactory.h"
#include "OgreParticleEmitterFactory.h"
#include "OgrePointEmitterFactory.h"
#include "WindowManager.h"
#include "BaseApplication.h"

using namespace OgreUtils;

void OgreUtils::destroyAllAttachedMovableObjects(SceneNode* node)
{
	if (!node) return;

	// Destroy all the attached objects
	SceneNode::ObjectIterator itObject = node->getAttachedObjectIterator();

	while (itObject.hasMoreElements())
		node->getCreator()->destroyMovableObject(itObject.getNext());

	// Recurse to child SceneNodes
	SceneNode::ChildNodeIterator itChild = node->getChildIterator();

	while (itChild.hasMoreElements())
	{
		SceneNode* pChildNode = static_cast<SceneNode*>(itChild.getNext());
		destroyAllAttachedMovableObjects(pChildNode);
	}
}

void OgreUtils::destroySceneNode(Ogre::SceneNode* node)
{
	if (!node) return;
	destroyAllAttachedMovableObjects(node);
	node->removeAndDestroyAllChildren();
	node->getCreator()->destroySceneNode(node);
}

void OgreUtils::destroySceneNodeChildren(Ogre::SceneNode* node)
{
	if (!node) return;
	destroyAllAttachedMovableObjects(node);
	node->removeAndDestroyAllChildren();
}


using namespace entitykernel;
using namespace Verse;

std::map<std::string, SystemEntityKernel*> EntityKernelManager::_systemMap;
std::map<MyVector3, SystemEntityKernel*, VectorLessThan> EntityKernelManager::_systemPosMap;
std::map<std::string, EntityKernel*> EntityKernelManager::_entityMap;
std::map<std::string, EntityKernel*> EntityKernelManager::_loneEntityMap;
int EntityKernelManager::_numSystems = 30;
Ogre::SceneManager * EntityKernelManager::_sceneMgr = 0;
int EntityKernelManager::seed = 24483234;
std::default_random_engine EntityKernelManager::_generator(seed);
std::list<std::future<void>* > EntityKernelManager::_systemThreadList;
Verse::Timer EntityKernelManager::_timer;
int EntityKernelManager::_gameMillisecondsPerTick = 50;
short int EntityKernelManager::_galaxyDirection = -1;
OrbitingEntityKernel * EntityKernelManager::_centerObject = 0;
std::list<SystemEntityKernel*> EntityKernelManager::_fullSystemList;
std::list<std::pair<Ogre::SceneNode*, Ogre::SceneNode*> > EntityKernelManager::_orbitSceneNodeList;
//bool EntityKernelManager::_renderingOrbits = true;
ParticleSystem * EntityKernelManager::_orbitStreamParticleSystem = 0;
double EntityKernelManager::_maxGalaxySpeed = 0.001; //units per tick
double EntityKernelManager::_minGalaxySpeed = 0.0005;
double EntityKernelManager::_gameSpeedFactor = 1.0; //by default game updates every _numMillisecondsPerTick, this factor speeds up or slows down the simulation
double EntityKernelManager::_gameYearsElapsed = 0.0;
int EntityKernelManager::_numGenerateTries = 500;
int EntityKernelManager::_recursionDepth = 1;
bool EntityKernelManager::isGeneratingGalaxy = false;
BaseApplication * EntityKernelManager::_mainApp = 0;
std::list<ThreadCommand*> EntityKernelManager::_threadCommandQueue;

EntityKernelManager::EntityKernelManager()
{

}

EntityKernelManager::~EntityKernelManager()
{
	delete _centerObject;
	DeleteSystemMap();
	ClearThreadList();
}

double EntityKernelManager::GetGalaxyRadius()
{
	//Default
	return 2000.00;
}

double EntityKernelManager::GetCenterObjectRadius()
{
	return GetGalaxyRadius()*0.05;
}

void EntityKernelManager::GenerateGalaxy()
{
	if(_sceneMgr != 0)
	{
		std::uniform_int_distribution<int> coinTossDist(0, 1);
		_galaxyDirection = coinTossDist(_generator) ? -1 : 1;

		GalaxyTextureGen::GenerateMaterials();

		//Generate the central black hole procedurally
		Procedural::SphereGenerator().setRadius((Real)GetCenterObjectRadius()).setUTile(40).setVTile(40).realizeMesh("CentralBlackHole");
		Ogre::Entity* ent2 = _sceneMgr->createEntity("CentralBlackHole");
		Ogre::SceneNode * node = _sceneMgr->getRootSceneNode()->createChildSceneNode();
		node->attachObject(ent2);
		Ogre::MaterialPtr material = GalaxyTextureGen::GenerateBlackHoleMaterial("CentralBlackHole");
		ent2->setMaterial(material);

		_centerObject = new OrbitingEntityKernel(BLACKHOLE, MyVector3(0,0,0), 1.0, GetCenterObjectRadius());
		_centerObject->SetName("CenterBlackHole");
		_centerObject->SetParentNode(node);
		_centerObject->_parentEntity = ent2;
		_centerObject->InitializeSelectionSystem();
		_centerObject->SetisCentralEntity(true);
		_loneEntityMap["CentralBlackHole"] = _centerObject;
		node->setUserAny(Ogre::Any(_centerObject));

		//double maxSystemRadius = std::min(GetGalaxyRadius()*0.1, (GetGalaxyRadius()*1.1)/(double)_numSystems);
		double maxSystemRadius = GetGalaxyRadius() * 0.01;
		double minSystemRadius = maxSystemRadius / 3.0;
		double maxStarMass = maxSystemRadius *2;
		double minStarMass = maxSystemRadius*1.2;
		double starR = 0.0;
		double starTheta = 0.0;
		double starXPos = 0.0;
		double starYPos = 0.0;
		double starZPos = 0.0;
		double starRadius = 0.0;
		double sysRadius = 0.0;
		double starMass = 0.0;
		short int yawDirection = 0;
		//short int pitchDirection = 0;
		double orbitVelocity = 0.0;
		MyVector3 starPos;
	
		std::uniform_real_distribution<double> rowdist(GetCenterObjectRadius(), EntityKernelManager::GetGalaxyRadius());
		std::uniform_real_distribution<double> thetadist(0, 2*M_PI);
		std::uniform_real_distribution<double> uvdist(1, 40);
		std::uniform_real_distribution<double> sysradiusdist(minSystemRadius, maxSystemRadius);
	
		SystemEntityKernel * systemEntity = 0;

		std::uniform_real_distribution<double> galaxyVelocityDist(_minGalaxySpeed, _maxGalaxySpeed);
		double galaxyOrbitVelocity = galaxyVelocityDist(_generator);
	
		for(int i = 0; i < EntityKernelManager::GetNumSystems(); i++)
		{
			//Generate the object kernel
			sysRadius = sysradiusdist(_generator);
			std::uniform_real_distribution<double> radiusdist(sysRadius*0.05, sysRadius*0.07);
			starRadius = radiusdist(_generator);

			std::uniform_real_distribution<double> massdist(GetMinMass(starRadius), GetMaxMass(starRadius));
			starMass = massdist(_generator);
			yawDirection = _galaxyDirection;
			//pitchDirection = coinTossDist(_generator) ? -1 : 1; //pitch is a percent of the yaw so orbits around z-axis have a small radius

			for (int i = 0; i < _numGenerateTries * 2; i++)
			{
				double starR = rowdist(_generator);
				orbitVelocity = ConvertVelocityToAngularVelocity(galaxyOrbitVelocity, starR);
				double starTheta = thetadist(_generator);
				double starXPos = starR*cos(starTheta);
				double starYPos = starR*sin(starTheta);

				std::uniform_real_distribution<double> zdist(-std::min(GetGalaxyRadius()*0.05, starR*0.5), std::min(GetGalaxyRadius()*0.05, starR*0.5));
				double starZPos = zdist(_generator);
				
				starPos = MyVector3((Real)starXPos, (Real)starYPos, (Real)starZPos);

				systemEntity = AddSystemEntity(entitykernel::STAR,
					sysRadius,
					starPos,
					starMass,
					starRadius);

				if (systemEntity != NULL)
					break;
			}
			
			std::string wasSuccessful = "No";
			if (systemEntity != NULL) wasSuccessful = "Yes";
	
			starXPos = starPos[0];
			starYPos = starPos[1];
			starZPos = starPos[2];
	
			if(systemEntity != NULL)
			{
				std::string circleName = "circle_" + systemEntity->GetName();
				//CreateCircleEntityWithRotation
				//This system has been successfully placed, now to generate the internal system objects
				OrbitingEntityKernel * starEntity = systemEntity->GetCentralEntity();
	
				if (starEntity != NULL)
				{
					//initialize the star object and create/set the ogre rendering node
					starEntity->SetOrbitParent(_centerObject);
					starEntity->SetOrbitVelocity(orbitVelocity);
					starEntity->_yawDirection = yawDirection;
		
					starEntity->renderDistance = sysRadius * 7;

					Ogre::MaterialPtr material = GalaxyTextureGen::GetRandomMaterial(starEntity->GetType());
					starEntity->_material = material;

					Ogre::SceneNode* sn = _sceneMgr->getRootSceneNode()->createChildSceneNode();
					sn->setUserAny(Ogre::Any(starEntity));
					starEntity->SetParentNode(sn);
					sn->translate(Ogre::Real(starXPos), Ogre::Real(starYPos), Ogre::Real(starZPos));
					
					//add sun flare particle system
					if (starEntity->GetType() == entitykernel::STAR)
					{
						/*
						std::string particleSystemName = starEntity->GetName() + "_SunFlare_particleSystem";
						ParticleSystem* sunParticlesClone = EntityKernelManager::GetSceneManager()->createParticleSystem(particleSystemName, "Space/Sun");
						sunParticlesClone->setKeepParticlesInLocalSpace(true);
						sunParticlesClone->setDefaultDimensions((Real)starEntity->GetRadius()*0.1, (Real)starEntity->GetRadius()*0.1);
						EllipsoidEmitter * emitter = dynamic_cast<EllipsoidEmitter*>(sunParticlesClone->getEmitter(0));
						Real particleVelocity = 25;
						if (emitter != 0)
						{
							emitter->setMaxTimeToLive((Real)GetNumTicksPerGameYear() / 2.0);
							emitter->setEmissionRate(30000);
							emitter->setSize((Real)starEntity->GetRadius()*2, (Real)starEntity->GetRadius() * 2, (Real)starEntity->GetRadius() * 2);
							emitter->setParticleVelocity(particleVelocity);
							emitter->setAngle(Radian(Real(M_PI)));
						}
						ParticleAffector * affector = sunParticlesClone->addAffector("LinearForce");
						LinearForceAffector * linearAffector = dynamic_cast<LinearForceAffector*>(affector);
						if (linearAffector != NULL)
						{
							linearAffector->setForceVector(Vector3(0, particleVelocity * 1, -particleVelocity * 4));
							linearAffector->setForceApplication(LinearForceAffector::ForceApplication::FA_ADD);
						}
						sn->attachObject(sunParticlesClone);
						sunParticlesClone->setRenderingDistance(starRadius * 5);
						sunParticlesClone->setEmitting(false);
						starEntity->_animatedTextureSystem = sunParticlesClone;
						*/

						starEntity->InitilaizeGlowSystem("Examples/Flare");
						starEntity->InitializeSelectionSystem();

						//orbit trail
						std::string OrbitSystemName = starEntity->GetName() + "_orbitTrail_particleSystem";
						ParticleSystem * orbitTrailSystem = EntityKernelManager::GetSceneManager()->createParticleSystem(OrbitSystemName);
						starEntity->_trailSystem = orbitTrailSystem;
						orbitTrailSystem->setMaterialName("Examples/Flare");
						orbitTrailSystem->setDefaultDimensions((Real)starEntity->GetRadius()*0.3, (Real)starEntity->GetRadius()*0.3);
						ParticleEmitter * pointEmitter = orbitTrailSystem->addEmitter("Point");
						pointEmitter->setEmissionRate(30);
						pointEmitter->setTimeToLive(10);
						pointEmitter->setParticleVelocity(0);
						sn->attachObject(orbitTrailSystem);
						orbitTrailSystem->setRenderingDistance(starRadius * 5);
						orbitTrailSystem->setEmitting(false);
					}
					//recursively generate the system objects
					systemEntity->GenerateSystem(0, true);
				}	
			}
		}
		PrintOrderedByPostitionSystemMap();
	}
	FindNearestNeighbors();
}

void EntityKernelManager::Inititalize(Ogre::SceneManager * sceneMgr, BaseApplication *mainApp)
{
	_sceneMgr = sceneMgr;
	_mainApp = mainApp;
}

void EntityKernelManager::DeleteSystemMap()
{
	std::map<std::string, SystemEntityKernel*>::iterator it;
	for (it = _systemMap.begin(); it != _systemMap.end(); it++)
	{
		delete it->second;
	}
	_systemMap.clear();
}

void EntityKernelManager::DeleteEntityMap()
{

}

EntityKernel * EntityKernelManager::GetEntity(std::string name)
{
	//no functionality yet
	return 0;
}

SystemEntityKernel *EntityKernelManager::GetSystemEntity(std::string name)
{
	SystemEntityKernel * entityKernel = 0;
	std::map<std::string, SystemEntityKernel*>::iterator itfind = _systemMap.find(name);
	if (itfind != _systemMap.end())
	{
		entityKernel = itfind->second;
	}
	return entityKernel;
}

std::string EntityKernelManager::GetUniqueName(std::string srcName)
{
	std::string newName = srcName;
	std::map<std::string, EntityKernel*>::iterator itfindEntity = _entityMap.find(srcName);
	if (itfindEntity != _entityMap.end() || newName == "") //found, not unique, create new name
	{
		newName = srcName + "_a" + std::to_string(_entityMap.size());
	}
	return newName;
}

void EntityKernelManager::AddKernelEntity(EntityKernel * kernel, std::string name)
{
	//no functionality yet
}

void EntityKernelManager::PrintOrderedByPostitionSystemMap()
{
	BaseEntityKernel * nearestEntity = 0;
	double minDistance = 0;
	double curEntityNearestNeighborDistance = 0;
	BaseEntityKernel *nearestNeighborNearestNeighbor = 0;
	MyVector3 pos;
	Real maxRadius = 0.0;
	double minDistanceBetween = 0;
	BaseEntityKernel * curEntity = 0;

	for (auto it = _systemPosMap.begin(); it != _systemPosMap.end(); it++)
	{
		SystemEntityKernel * curSystem = it->second;
		EntityKernel * centralEntity = curSystem->GetCentralEntity();
		if(centralEntity != NULL)
		{
			pos = curSystem->GetPosition();
			std::cout << "Star Name: " << curSystem->GetName() << ", System Name: " << curSystem->GetName() << std::endl;
			std::cout << "System Radius:" << curSystem->GetRadius() << ", Star Radius: " << centralEntity->GetRadius() << std::endl;
			std::cout << "Position : " << pos << std::endl;

			std::cout << "Nearest Neighbor: " << std::endl;
			BaseEntityKernel * nearestNeighbor = curSystem->GetNearestNeighbor();
			if(nearestNeighbor != 0)
			{
				MyVector3 nPos = nearestNeighbor->GetPosition();
				std::cout << "Position: " << nPos << std::endl;
				std::cout << "Distance Between Centers: " << nPos.distance(pos) << std::endl << std::endl;
			}
			else std::cout << "Not Available" << std::endl << std::endl;

			/*
			for (auto itSystem = it->second->_loneEntityMap.begin(); itSystem != it->second->_loneEntityMap.end(); itSystem++)
			{
				EntityKernel * curLoneEntity = itSystem->second;
				pos = curLoneEntity->GetPosition();
				std::cout << "Internal Lone Entity Name: " << curLoneEntity->GetName() << std::endl;
				std::cout << "Position: " << pos << std::endl;
				GetDerivedParentPosition(curLoneEntity, pos);
				std::cout << "Derived World Position: " << pos << std::endl << std::endl;
			}
			for (auto itSystem1 = it->second->_systemMap.begin(); itSystem1 != it->second->_systemMap.end(); itSystem1++)
			{
				SystemEntityKernel * curLoneEntity = itSystem1->second;
				pos = curLoneEntity->GetPosition();
				std::cout << "Internal System Name: " << curLoneEntity->GetName() << std::endl;
				std::cout << "Position: " << pos << std::endl;
				GetDerivedParentPosition(curLoneEntity, pos);
				std::cout << "Derived World Position: " << pos << std::endl;
			}
			*/
		}
	}
}

//Place the star system object, returns true if the object was successfully placed
bool EntityKernelManager::AddSystemEntity(SystemEntityKernel * kernel, std::string name)
{
	if(kernel != 0)
	{
		MyVector3 systemPos = kernel->GetPosition();
		double systemRadius = kernel->GetRadius();

		BaseEntityKernel * nearestEntity = 0;
		double minDistance = 0;
		double curEntityNearestNeighborDistance = 0;
		BaseEntityKernel *nearestNeighborNearestNeighbor = 0;
		MyVector3 curPos;
		double maxRadius = 0.0;
		double minRadius = 0.0;
		double minDistanceBetween = 0;
		BaseEntityKernel * curEntity = 0;
		Real distanceBetween = 0.0;
		double row = systemPos.distance(Vector3(0, 0, 0));
		double rowCurPos = 0.0;
		//double distanceBetweenRow = 0.0;
		//double distanceZ = 0.0;
			
		std::map<MyVector3, SystemEntityKernel*>::iterator kernelfind = _systemPosMap.find(systemPos);
		if(kernelfind != _systemPosMap.end()) //an entity alread exists here, try again
		{
			return false;
		}

		//check that this object is far enough from the galactic center
		maxRadius = std::max(systemRadius, GetCenterObjectRadius());
		minRadius = std::min(systemRadius, GetCenterObjectRadius());
		distanceBetween = systemPos.distance(MyVector3(0,0,0));
		minDistanceBetween = maxRadius + minRadius * 1.2;
		if (distanceBetween < minDistanceBetween)
		{
			return false;
		}

		//Check for overlapps, systems cannot be too close
		for(auto it = _systemMap.begin(); it != _systemMap.end(); it++)
		{
			curEntity = it->second;
			if(curEntity != 0)
			{
				curPos = curEntity->GetPosition();
				maxRadius = std::max(systemRadius, curEntity->GetRadius());
				distanceBetween = systemPos.distance(curPos);
				minDistanceBetween = maxRadius*3;
				rowCurPos = curPos.distance(Vector3(0, 0, 0));
				//distanceBetweenRow = std::abs(row - rowCurPos);
				//distanceZ = abs(curPos[2] - systemPos[2]);
				maxRadius = std::max(systemRadius, curEntity->GetRadius());
				if (distanceBetween < minDistanceBetween)
				{
					return false;
				}
			}
			else return false;
		}
		_systemPosMap[systemPos] = kernel;
		_fullSystemList.push_back(kernel);
		std::string nameUnique = GetUniqueName(name);
		kernel->SetName(nameUnique);
		_systemMap[nameUnique] = kernel;
		EntityKernel * centralEntity = kernel->GetCentralEntity();
		if (centralEntity != NULL)
		{
			kernel->_loneEntityMap[nameUnique] = centralEntity;
			kernel->_loneEntityPosMap[centralEntity->GetPosition()] = centralEntity;
			_entityMap[nameUnique] = centralEntity;
			centralEntity->SetName(nameUnique);
		}

		return true;
	}
	else return false;
}

//update the objects, this is threaded and launches the physics calculations for each system asychronously
void EntityKernelManager::UpdateEntitiesForTimeStep()
{
	_timer.ResetTimer(); //reset the sim timer
	FindNearestNeighborsThread();

	for (auto it = _fullSystemList.begin(); it != _fullSystemList.end(); it++)
	{
		SystemEntityKernel * kernel = (*it);
		kernel->ClearThreadManagementLists();
		std::future<void> * newThreadFuture = new std::future<void>(std::async(std::launch::async, [kernel]() { kernel->UpdateEntityForTimeStep(); }));
		_systemThreadList.push_back(newThreadFuture); //add this thread to list for tracking
	}
}

bool EntityKernelManager::RemoveSystemEntity(std::string name)
{
	bool removeSucc = false;
	std::map<std::string, SystemEntityKernel*>::iterator itfind = _systemMap.find(name);
	if (itfind != _systemMap.end())
	{
		removeSucc = true;
		delete itfind->second;
		_systemMap.erase(itfind);
	}
	return removeSucc;
}

bool EntityKernelManager::RemoveKernelEntity(std::string name)
{
	return false;
	//no functionality yet
}

bool EntityKernelManager::RemoveKernelEntity(EntityKernel * kernel)
{
	return RemoveKernelEntity(kernel->GetName());
}

bool EntityKernelManager::RemoveSystemEntity(SystemEntityKernel * kernel)
{
	return RemoveSystemEntity(kernel->GetName());
}

void EntityKernelManager::ChangeSystemName(std::string orgName, std::string newName)
{
	if (orgName == newName)
		return;

	std::map<std::string, SystemEntityKernel*>::iterator itfind = _systemMap.find(orgName);
	if (itfind != _systemMap.end()) //found so move this value to new key
	{
		std::string uniqueName = GetUniqueName(newName);
		_systemMap[uniqueName] = itfind->second;
		_systemMap.erase(itfind);
	}
}

void EntityKernelManager::ChangeEntityName(std::string orgName, std::string newName)
{
	//no functionality yet
}

EntityKernel * EntityKernelManager::AddKernelEntity(entitykernel::EntityType type,
	MyVector3 position,
	double mass, 
	double radius,
	bool isStatic, 
	std::string name)
{
	//no functionality yet
	return 0;
}

//attempts to add a new system, if not successful returns NULL
SystemEntityKernel * EntityKernelManager::AddSystemEntity(entitykernel::EntityType centerEntitytype,
	double sysRadius,
	MyVector3 centerPos,
	double centerMass,
	double centerObjectRadius,
	std::string name)
{
	SystemEntityKernel * systemEntity = new SystemEntityKernel(centerEntitytype,
		centerPos,
		centerMass,
		centerObjectRadius,
		sysRadius,
		name);

	if (!AddSystemEntity(systemEntity, name))
	{
		delete systemEntity;
		return 0;
	}
	return systemEntity;
}

//all objects positions are relative to their parent systems, this returns the absolute position based on the input relative position
void EntityKernelManager::GetDerivedParentPosition(BaseEntityKernel* childKernel, MyVector3 &derivedPos)
{
	if (childKernel != NULL)
	{
		BaseEntityKernel * parentKernel = childKernel->GetParentKernel();
		if (parentKernel != NULL)
		{
			Vector3 derivedPos1 = derivedPos + parentKernel->GetPosition();
			derivedPos = MyVector3(derivedPos1[0], derivedPos1[1], derivedPos1[2]);
			GetDerivedParentPosition(parentKernel, derivedPos);
		}
	}
}

//this function runs all the commands in the commands list, has not been implemented
void EntityKernelManager::UpdateFromSystemThreadOutputs()
{
	for (auto commandIt = _threadCommandQueue.begin(); commandIt != _threadCommandQueue.end(); commandIt++)
	{
		(*commandIt)->Process();
	}

	std::list<SystemEntityKernel*>::iterator it;
	for (it = _fullSystemList.begin(); it != _fullSystemList.end(); it++)
	{
		SystemEntityKernel * kernel = (*it);
		//if(kernel->GetParentKernel() == NULL) //for debugging purposes
			kernel->UpdateFromSystem();
	}
}

void EntityKernelManager::UpdateOrbitTrailRendering(bool render)
{
	for (auto it = _entityMap.begin(); it != _entityMap.end(); it++)
	{
		ParticleSystem * trailSystem = it->second->_trailSystem;
		if (trailSystem != NULL)
		{
			if (render)
			{
				it->second->trailsOn = true;
				//trailSystem->setEmitting(true);
			}
			else
			{
				it->second->trailsOn = false;
				//trailSystem->setEmitting(false);
			}
		}
	}
}

void EntityKernelManager::RemoveGalaxy()
{
	delete _centerObject;

	WindowManager::ResetInfo();
	_threadCommandQueue.clear();
	DeleteSystemMap();
	_loneEntityMap.clear();
	_entityMap.clear();
	_systemPosMap.clear();
	_fullSystemList.clear();
	ClearThreadList();
	_gameYearsElapsed = 0;
	_gameSpeedFactor = 1;
	_timer.ResetTimer();
	_generator = std::default_random_engine(seed);
}

void EntityKernelManager::InitializeParticleSystems()
{
	EllipsoidEmitterFactory * ellipsoidEmitter = new EllipsoidEmitterFactory();
	LinearForceAffectorFactory * linearAffector = new LinearForceAffectorFactory();
	PointEmitterFactory * pointEmitter = new PointEmitterFactory();
	ParticleSystemManager::getSingleton().addAffectorFactory(linearAffector);
	ParticleSystemManager::getSingleton().addEmitterFactory(ellipsoidEmitter);
	ParticleSystemManager::getSingleton().addEmitterFactory(pointEmitter);
}

void EntityKernelManager::RemoveMaterials()
{
	MaterialManager * materials = MaterialManager::getSingletonPtr();
	std::string name = "procedural_CentralBlackHole_Material";
	materials->remove(name);

	for (auto it = GalaxyTextureGen::_planetMaterialList.begin(); it != GalaxyTextureGen::_planetMaterialList.end(); it++)
	{
		materials->remove((*it));
	}

	for (auto it1 = GalaxyTextureGen::_starMaterialList.begin(); it1 != GalaxyTextureGen::_starMaterialList.end(); it1++)
	{
		materials->remove((*it1));
	}

	TextureManager::getSingletonPtr()->removeAll();
}

void EntityKernelManager::RenderOrbitPath(OrbitingEntityKernel * entity)
{
	if (entity != NULL)
	{
		if (!entity->_orbitRendered)
		{
			double radius = entity->GetRadius();
			OrbitingEntityKernel * orbitParentKernel = entity->_orbitParent;
			SceneNode * orbitNode = entity->_orbitSceneNode;

			double parentSystemRadius = EntityKernelManager::GetGalaxyRadius();

			if (orbitParentKernel != NULL && orbitNode != NULL)
			{
				SystemEntityKernel *systemKernel = orbitParentKernel->GetParentKernel();
				if (systemKernel != NULL)
				{
					parentSystemRadius = systemKernel->GetRadius();
				}

				SceneNode * orbitParentNode = orbitParentKernel->GetRendererNode();
				if (orbitParentNode != NULL)
				{
					std::list<std::pair<std::string, Ogre::Vector3> > &orbitPath = entity->_orbitPath;
					for (auto it = orbitPath.begin(); it != orbitPath.end(); it++)
					{
						Ogre::SceneNode * childNode = orbitNode->createChildSceneNode();
						Procedural::SphereGenerator().setRadius((Real)(radius*0.1)).setUTile(1).setVTile(1).realizeMesh(it->first);
						Ogre::Entity* ent2 = EntityKernelManager::GetSceneManager()->createEntity(it->first);
						childNode->attachObject(ent2);
						childNode->translate(it->second);

						ent2->setRenderingDistance(parentSystemRadius * 5);
					}
					orbitParentNode->addChild(orbitNode);
				}
			}
			entity->_orbitRendered = true;
		}
	}
}

void EntityKernelManager::StopRenderOrbitPath(OrbitingEntityKernel * entity)
{
	if (entity != NULL)
	{
		if (entity->_orbitRendered)
		{
			OrbitingEntityKernel * orbitParentKernel = entity->_orbitParent;
			SceneNode * orbitNode = entity->_orbitSceneNode;
			if (orbitParentKernel != NULL && orbitNode)
			{
				SceneNode * orbitParentNode = orbitParentKernel->GetRendererNode();
				if (orbitParentNode != NULL)
				{
					orbitParentNode->removeChild(orbitNode);
					destroySceneNodeChildren(orbitNode);
					std::list<std::pair<std::string, Ogre::Vector3> > &orbitPath = entity->_orbitPath;
					for (auto it = orbitPath.begin(); it != orbitPath.end(); it++)
					{
						MeshManager::getSingleton().remove(it->first);
					}
				}
			}
			entity->_orbitRendered = false;
		}
	}
}

void EntityKernelManager::StopRendereringAllOrbits()
{
	StopRenderingOrbits(_centerObject);

	for (auto it = _fullSystemList.begin(); it != _fullSystemList.end(); it++)
	{
		OrbitingEntityKernel * centralEntity = (*it)->GetCentralEntity();
		if (centralEntity != NULL)
		{
			StopRenderingOrbits(centralEntity);
		}
	}
}

void EntityKernelManager::StopRenderingOrbits(OrbitingEntityKernel * centralEntity)
{
	if (centralEntity != NULL)
	{
		if (centralEntity->_orbitsRendered)
		{
			centralEntity->_orbitsRendered = false;

			SceneNode * parentOrbitNode = centralEntity->GetRendererNode();
			parentOrbitNode->removeAllChildren();

			SceneNode * orbitNode = 0;
			OrbitingEntityKernel * orbitingEntity = 0;
			if (centralEntity->isCentralEntity() && parentOrbitNode != NULL)
			{
				std::map<std::string, EntityKernel*> &loneEntities = std::map<std::string, EntityKernel*>();
				std::map<std::string, SystemEntityKernel*> &systemEntities = EntityKernelManager::_systemMap;
				SystemEntityKernel * parentSystem = centralEntity->GetParentKernel();
				if (parentSystem != NULL)
				{
					loneEntities = parentSystem->_loneEntityMap;
					systemEntities = parentSystem->_systemMap;
				}
				for (auto loneIt = loneEntities.begin(); loneIt != loneEntities.end(); loneIt++)
				{
					EntityKernel * entity = loneIt->second;
					orbitingEntity = dynamic_cast<OrbitingEntityKernel*>(entity);
					if (orbitingEntity != NULL && orbitingEntity != centralEntity && orbitingEntity->_orbitRendered)
					{
						orbitingEntity->_orbitRendered = false;
						orbitNode = orbitingEntity->_orbitSceneNode;
						destroySceneNodeChildren(orbitNode);

						std::list<std::pair<std::string, Ogre::Vector3> > &orbitPath = orbitingEntity->_orbitPath;

						for (auto it = orbitPath.begin(); it != orbitPath.end(); it++)
						{
							MeshManager::getSingleton().remove(it->first);
						}
					}
				}

				for (auto systemIt = systemEntities.begin(); systemIt != systemEntities.end(); systemIt++)
				{
					orbitingEntity = systemIt->second->GetCentralEntity();
					if (orbitingEntity != NULL  && orbitingEntity->_orbitRendered)
					{
						centralEntity->_orbitsRendered = false;
						orbitNode = orbitingEntity->_orbitSceneNode;
						destroySceneNodeChildren(orbitNode);
						std::list<std::pair<std::string, Ogre::Vector3> > &orbitPath = orbitingEntity->_orbitPath;
						for (auto it = orbitPath.begin(); it != orbitPath.end(); it++)
						{
							MeshManager::getSingleton().remove(it->first);
						}
						orbitingEntity->_orbitRendered = false;
					}
				}
			}
		}
	}
}

void EntityKernelManager::RenderOrbits(OrbitingEntityKernel * centralEntity)
{
	if (centralEntity != NULL)
	{
		if (!centralEntity->_orbitsRendered)
		{
			centralEntity->_orbitsRendered = true;

			SceneNode * parentOrbitNode = centralEntity->GetRendererNode();
			SceneNode * orbitNode = 0;
			OrbitingEntityKernel * orbitingEntity = 0;
			double radius = 0;
			double parentSystemRadius = GetGalaxyRadius();
			if (centralEntity->isCentralEntity() && parentOrbitNode != NULL)
			{
				std::map<std::string, EntityKernel*> &loneEntities = std::map<std::string, EntityKernel*>();
				std::map<std::string, SystemEntityKernel*> &systemEntities = EntityKernelManager::_systemMap;
				SystemEntityKernel * parentSystem = centralEntity->GetParentKernel();
				if (parentSystem != NULL)
				{
					loneEntities = parentSystem->_loneEntityMap;
					systemEntities = parentSystem->_systemMap;
					parentSystemRadius = parentSystem->GetRadius();
				}

				for (auto loneIt = loneEntities.begin(); loneIt != loneEntities.end(); loneIt++)
				{
					EntityKernel * entity = loneIt->second;
					orbitingEntity = dynamic_cast<OrbitingEntityKernel*>(entity);
					if (orbitingEntity != NULL && orbitingEntity != centralEntity && !orbitingEntity->_orbitRendered)
						//if (orbitingEntity->_rotationAnglesSet && orbitingEntity != centralEntity && !orbitingEntity->_orbitRendered)
					{
						orbitNode = orbitingEntity->_orbitSceneNode;
						radius = orbitingEntity->GetRadius();
						std::list<std::pair<std::string, Ogre::Vector3> > &orbitPath = orbitingEntity->_orbitPath;

						for (auto it = orbitPath.begin(); it != orbitPath.end(); it++)
						{
							Ogre::SceneNode * childNode = orbitNode->createChildSceneNode();
							Procedural::SphereGenerator().setRadius((Real)(radius*0.1)).setUTile(1).setVTile(1).realizeMesh(it->first);
							Ogre::Entity* ent2 = EntityKernelManager::GetSceneManager()->createEntity(it->first);
							childNode->attachObject(ent2);
							childNode->translate(it->second);

							ent2->setRenderingDistance(parentSystemRadius * 5);
						}
						//render this entities orbit path
						orbitingEntity->_orbitRendered = true;
						parentOrbitNode->addChild(orbitNode);
					}
				}

				for (auto systemIt = systemEntities.begin(); systemIt != systemEntities.end(); systemIt++)
				{
					orbitingEntity = systemIt->second->GetCentralEntity();
					if (orbitingEntity != NULL && !orbitingEntity->_orbitRendered)
					{
						//if (orbitingEntity->_rotationAnglesSet && !orbitingEntity->_orbitRendered)
						orbitNode = orbitingEntity->_orbitSceneNode;
						std::list<std::pair<std::string, Ogre::Vector3> > &orbitPath = orbitingEntity->_orbitPath;
						for (auto it = orbitPath.begin(); it != orbitPath.end(); it++)
						{
							radius = orbitingEntity->GetRadius();
							Ogre::SceneNode * childNode = orbitNode->createChildSceneNode();
							Procedural::SphereGenerator().setRadius((Real)(radius*0.1)).setUTile(1).setVTile(1).realizeMesh(it->first);
							Ogre::Entity* ent2 = EntityKernelManager::GetSceneManager()->createEntity(it->first);
							childNode->attachObject(ent2);
							childNode->translate(it->second);

							ent2->setRenderingDistance(parentSystemRadius * 5);
						}
						orbitingEntity->_orbitRendered = true;
						parentOrbitNode->addChild(orbitNode);
					}
				}
			}
		}
	}
}

void EntityKernelManager::FindNearestNeighbors()
{
	//go through lone entities first
	for (auto loneIt = _loneEntityMap.begin(); loneIt != _loneEntityMap.end(); loneIt++)
	{
		loneIt->second->FindNearestNeighbor();
	}

	//then go through system entities
	for (auto systemIt = _systemMap.begin(); systemIt != _systemMap.end(); systemIt++)
	{
		systemIt->second->FindNearestNeighbor();
		systemIt->second->FindNearestNeighbors(); //recursively find nearest neighbors of children
	}
}

void EntityKernelManager::FindNearestNeighborsThread()
{
	//go through lone entities first
	for (auto loneIt = _loneEntityMap.begin(); loneIt != _loneEntityMap.end(); loneIt++)
	{
		loneIt->second->FindNearestNeighbor();
	}

	//then go through system entities
	for (auto systemIt = _systemMap.begin(); systemIt != _systemMap.end(); systemIt++)
	{
		systemIt->second->FindNearestNeighbor();
	}
}

