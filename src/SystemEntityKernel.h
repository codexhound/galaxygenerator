#ifndef __SYSTEMENTITY_H__
#define __SYSTEMENTITY_H__
#include "EntityKernel.h"
#include "ThreadContainers.h"
#include <map>

class ThreadCommand;

class SystemEntityKernel : public BaseEntityKernel
{
public:
	SystemEntityKernel(OrbitingEntityKernel * centerEntity = 0, double radius = 40, std::string name = "");
	SystemEntityKernel(entitykernel::EntityType centerEntitytype,
		MyVector3 centerPosition = MyVector3(0.0, 0.0, 0.0),
		double centerMass = 1.0, double centerEntityRadius = 1.0, double sysRadius = 1.0,
		std::string name = "");
	~SystemEntityKernel();

	OrbitingEntityKernel * GetCentralEntity();
	void UpdateEntityForTimeStep(); //thread worker update function for this class

	//Adds an orbit entity, auto creates the constructor
	//Returns a pointer to the created orbit entity
	OrbitingEntityKernel * AddOrbitingEntity(EntityType type = EntityType::STAR, 
		MyVector3 position = MyVector3(0.0, 0.0, 0.0),
		double mass = 1.0, 
		double radius = 1.0,
		std::string name = "");
	bool AddOrbitingEntity(OrbitingEntityKernel * kernel, std::string name);
	void AddEntity(EntityKernel* kernel, std::string name = "");
	EntityKernel * GetEntity(std::string name);

	SystemEntityKernel * AddSystemEntity(entitykernel::EntityType centerEntitytype,
		double sysRadius,
		MyVector3 centerPos,
		double centerMass,
		double centerObjectRadius,
		std::string name = "");

	bool AddSystemEntity(SystemEntityKernel * kernel, std::string name = "");
	short int _systemDirection; //positive or negative

	void GenerateSystem(int curRecursionDepth, bool internalSystemsAllowed = false);

	//System Generation Helpers
	double OrbitObjectRadiusMax();
	double OrbitObjectRadiusMin();
	double InternalSystemRadiusMax();
	double InternalSystemRadiusMin();
	double InternalObjectMassMax();
	double InternalObjectMassMin();
	double GetMinDistanceFromCenter();
	double GetMaxZDistance();

	//nearest neighbor non thread safe
	void FindNearestNeighbors();

	//nearest neighbor thread
	void FindNearestNeighborsThread();

	//Map of Entities in the system
	std::map<std::string, EntityKernel*> _loneEntityMap; //map of lone objects, dont count as systems
	std::map<MyVector3, EntityKernel*, VectorLessThan> _loneEntityPosMap;
	std::map<std::string, SystemEntityKernel*> _systemMap; //map of children systems
	std::map<MyVector3, SystemEntityKernel*, VectorLessThan> _systemPosMap; //ordered by pos, only a reference for quickly finding system at location

	//thread management and data queing
	std::list < std::pair<EntityKernel*, MyVector3> > newPositionList; //list of kernels with new positions
	std::list <EntityKernel*> newEntityList; //list of new kernels to render
	std::list <EntityKernel*> deletedEntityList; //list of new kernels to remove from rendering
	std::list <OrbitNoteObject> entityContainerList; //list of new orbit objects to render
	std::list <OrbitingEntityKernel*> orbitMeshGenerationList; //list of new orbit objects to generate

	std::list <ThreadCommand*> _threadCommandQueue;

	void ClearThreadManagementLists()
	{
		newPositionList.clear();
		newEntityList.clear();
		deletedEntityList.clear();
		entityContainerList.clear();
		orbitMeshGenerationList.clear();
	}

	void UpdateFromSystem(); //Updates from the thread values in this system, then calls children

	void GenerateFullSystemListChild();

private:

	void RemoveChildSystems();
	void RemoveChildEntities();
	OrbitingEntityKernel * _centralEntity; //entity at the center

	static int _numSystemObjectsMax;
	static int _numSystemsWithinMax;
};

#endif
