#include "SystemEntityKernel.h"
#include "EntityKernelManager.h"
#include <random>
#include <iostream>
#include "Procedural.h"
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <thread>
#include <future>
#include "MeshGenerator.h"
#include "MathDefines.h"
#include "OgrePointEmitter.h"
#include "BaseApplication.h"

using namespace Verse;

int SystemEntityKernel::_numSystemObjectsMax = 20;
int SystemEntityKernel::_numSystemsWithinMax = SystemEntityKernel::_numSystemObjectsMax/2;

SystemEntityKernel::SystemEntityKernel(OrbitingEntityKernel * centerEntity, double radius, std::string name) : BaseEntityKernel()
{
	_centralEntity = centerEntity;
	_radius = radius;
	_name = name;
	_systemDirection = -1;
	_startingPosition = MyVector3(0,0,0);
	_startingTheta = 0;
	_lastTheta = 0;
}

SystemEntityKernel::SystemEntityKernel(entitykernel::EntityType centerEntitytype,
	MyVector3 centerPosition,
	double centerMass,
	double centerEntityRadius, double sysRadius,
	std::string name) : BaseEntityKernel()
{
	OrbitingEntityKernel * centerEntity = new OrbitingEntityKernel(centerEntitytype,
		MyVector3(0.0, 0.0, 0.0), //relative to systems position
		centerMass,
		centerEntityRadius);

	this->SetType(centerEntitytype);
	_systemDirection = -1;
	centerEntity->SetisCentralEntity(true);
	centerEntity->SetParentKernel(this);
	_position = centerPosition;
	_startingPosition = _position;
	_startingTheta = 0;
	_lastTheta = _startingTheta;

	_radius = sysRadius;
	_centralEntity = centerEntity;
}

SystemEntityKernel::~SystemEntityKernel()
{
	RemoveChildSystems();
	RemoveChildEntities();
}

void SystemEntityKernel::RemoveChildSystems()
{
	for (auto it = _systemMap.begin(); it != _systemMap.end(); it++)
	{
		delete it->second;
	}
}

void SystemEntityKernel::RemoveChildEntities()
{
	for(auto it = _loneEntityMap.begin(); it != _loneEntityMap.end(); it++)
	{
		delete it->second;
	}
}

double SystemEntityKernel::OrbitObjectRadiusMax()
{
	double radius = 0;
	if (_centralEntity != NULL)
		radius = _centralEntity->GetRadius()*0.1;

	return radius;
}

double SystemEntityKernel::OrbitObjectRadiusMin()
{
	double radius = 0;
	if (_centralEntity != NULL)
		radius = _centralEntity->GetRadius()*0.005;

	return radius;
}

double SystemEntityKernel::InternalSystemRadiusMax()
{
	return _radius * 0.05;
}

double SystemEntityKernel::InternalSystemRadiusMin()
{
	return _radius * 0.009;
}

double SystemEntityKernel::InternalObjectMassMax()
{
	double mass = 0;
	if (_centralEntity != NULL)
		mass = _centralEntity->GetMass()*.05;

	return mass;
}

double SystemEntityKernel::InternalObjectMassMin()
{
	double mass = 0;
	if (_centralEntity != NULL)
		mass = _centralEntity->GetMass()*.005;

	return mass;
}

double SystemEntityKernel::GetMinDistanceFromCenter()
{
	double row = 0;
	if (_centralEntity != NULL)
		row = _centralEntity->GetRadius() * 1.25;

	return row;
}

double SystemEntityKernel::GetMaxZDistance()
{
	double zMax = 0;
	if (_centralEntity != NULL)
		zMax = std::min(_centralEntity->GetRadius() * 3, _radius*0.05);

	return zMax;
}

void SystemEntityKernel::GenerateSystem(int curRecursionDepth, bool interalSystemsAllowed)
{
	int totalNumberInteralSystems = 0;

	double objectRow = 0.0;
	double objectTheta = 0.0;
	double objectXPos = 0.0;
	double objectYPos = 0.0;
	double objectZPos = 0.0;
	double objectRadius = 0.0;
	double sysRadius = 0.0;
	double objectMass = 0.0;
	int isSystem = 0;
	double orbitVelocity = 0.0;
	short int yawDirection = 0;
	short int pitchDirection = 0;
	MyVector3 ObjectPos; //this is relative to the systems position
	double uTile = 0.0;
	double vTile = 0.0;

	SystemEntityKernel * systemKernel = 0;
	OrbitingEntityKernel * entityKernel = 0;

	std::uniform_int_distribution<int> isSystemDist(0, 1);
	std::uniform_int_distribution<short int> coinTossDist(0, 1);
	std::uniform_real_distribution<double> massdist(InternalObjectMassMin(), InternalObjectMassMax());
	std::uniform_int_distribution<int> numSystemObjectsDist(1, _numSystemObjectsMax);
	std::uniform_int_distribution<int> numInternalSystemsDist(1, _numSystemsWithinMax);
	std::uniform_real_distribution<double> thetadist(0.1, 2*M_PI);
	std::uniform_real_distribution<double> uvdist(1, 40);
	std::uniform_real_distribution<double> radiusdist(OrbitObjectRadiusMin(), OrbitObjectRadiusMax());
	std::uniform_real_distribution<double> sysradiusdist(InternalSystemRadiusMin(), InternalSystemRadiusMax());

	int numInternalSystems = numInternalSystemsDist(EntityKernelManager::_generator);
	int numSystemObjects = numSystemObjectsDist(EntityKernelManager::_generator);

	_systemDirection = coinTossDist(EntityKernelManager::_generator) ? -1 : 1;

	for (int i = 0; i < numSystemObjects; i++)
	{
		objectRadius = radiusdist(EntityKernelManager::_generator);
		std::uniform_real_distribution<double> rowdist(this->GetMinDistanceFromCenter(), (_radius- objectRadius));
		isSystem = isSystemDist(EntityKernelManager::_generator);
		sysRadius = sysradiusdist(EntityKernelManager::_generator);
		objectMass = massdist(EntityKernelManager::_generator);
		yawDirection = _systemDirection;
		pitchDirection = coinTossDist(EntityKernelManager::_generator) ? -1 : 1;
		uTile = uvdist(EntityKernelManager::_generator);
		vTile = uvdist(EntityKernelManager::_generator);

		for (int tryCount = 0; tryCount < EntityKernelManager::_numGenerateTries; tryCount++)
		{
			systemKernel = 0;
			entityKernel = 0;

			objectTheta = thetadist(EntityKernelManager::_generator);
			objectRow = rowdist(EntityKernelManager::_generator);
			objectXPos = objectRow*std::cos(objectTheta);
			objectYPos = objectRow*std::sin(objectTheta);
			std::uniform_real_distribution<double> zdist(-std::min(this->GetMaxZDistance(), objectRow*0.5), std::min(this->GetMaxZDistance(), objectRow*0.5));
			objectZPos = zdist(EntityKernelManager::_generator);
			ObjectPos = MyVector3((Real)objectXPos, (Real)objectYPos, (Real)objectZPos); //this is relative to the systems position

			std::uniform_real_distribution<double> orbitVelocityDegreesPerTick(EntityKernelManager::GetMinOrbitVelocity(objectRow), EntityKernelManager::GetMaxOrbitVelocity(objectRow));
			orbitVelocity = orbitVelocityDegreesPerTick(EntityKernelManager::_generator);

			EntityType type = PLANET;
			if (curRecursionDepth > 0)
				type = entitykernel::MOON;

			if (isSystem && totalNumberInteralSystems < numInternalSystems && curRecursionDepth < EntityKernelManager::_recursionDepth) //dont generate systems when depth gets to a certain point
			{
				//attempt system creation
				systemKernel = AddSystemEntity(type,
					sysRadius,
					ObjectPos,
					objectMass,
					objectRadius);

				//continue if placement successful
				if (systemKernel != NULL)
				{
					totalNumberInteralSystems++;
					entityKernel = systemKernel->GetCentralEntity();
					break;
				}	
			}
			else //make just a lone system object
			{
				entityKernel = AddOrbitingEntity(
					type,
					ObjectPos, 
					objectMass, 
					objectRadius);

				if (entityKernel != NULL)
				{
					break;
				}
			}
		}
		if (entityKernel != NULL)
		{
			entityKernel->_yawDirection = yawDirection;
			entityKernel->SetOrbitParent(this->GetCentralEntity());
			entityKernel->SetOrbitVelocity(orbitVelocity);
			MyVector3 entityPosition = entityKernel->GetPosition();
			EntityKernelManager::GetDerivedParentPosition(entityKernel, entityPosition);
			//entity position has now been converted into a world position
			Ogre::SceneManager * sceneMgr = EntityKernelManager::GetSceneManager();
			if (sceneMgr != NULL)
			{
				Ogre::MaterialPtr material = GalaxyTextureGen::GetRandomMaterial(entityKernel->GetType());

				entityKernel->_material = material;
				entityKernel->renderDistance = _radius * 5;

				Ogre::SceneNode* sn = sceneMgr->getRootSceneNode()->createChildSceneNode();
				sn->setUserAny(Ogre::Any(entityKernel));
				entityKernel->SetParentNode(sn);
				sn->translate(entityPosition[0], entityPosition[1], entityPosition[2]);

				//orbit trail
				std::string OrbitSystemName = entityKernel->GetName() + "_orbitTrail_particleSystem";
				ParticleSystem * orbitTrailSystem = EntityKernelManager::GetSceneManager()->createParticleSystem(OrbitSystemName);
				entityKernel->_trailSystem = orbitTrailSystem;
				orbitTrailSystem->setMaterialName("Examples/Flare");
				orbitTrailSystem->setDefaultDimensions(entityKernel->GetRadius()*0.1, entityKernel->GetRadius()*0.1);
				ParticleEmitter * emitter = orbitTrailSystem->addEmitter("Point");
				emitter->setEmissionRate(30);
				emitter->setTimeToLive(10);
				emitter->setParticleVelocity(0);
				orbitTrailSystem->setRenderingDistance(objectRadius * 5);
				orbitTrailSystem->setEmitting(false);

				sn->attachObject(orbitTrailSystem);

				entityKernel->InitializeSelectionSystem();

				//This system has been successfully placed, now to generate the internal system objects
				if (curRecursionDepth < EntityKernelManager::_recursionDepth && systemKernel != NULL) //curRecursionDepth must be less than the max depth
				{
					systemKernel->GenerateSystem(curRecursionDepth + 1, true);
				}
					
			}
		}
	}
}

SystemEntityKernel * SystemEntityKernel::AddSystemEntity(entitykernel::EntityType centerEntitytype,
	double sysRadius,
	MyVector3 centerPos,
	double centerMass,
	double centerObjectRadius,
	std::string name)
{
	SystemEntityKernel * systemEntity = new SystemEntityKernel(centerEntitytype,
		centerPos,
		centerMass,
		centerObjectRadius,
		sysRadius,
		name);

	if (!AddSystemEntity(systemEntity, name))
	{
		delete systemEntity;
		return 0;
	}
	return systemEntity;
}


//attempt to place a system entity, returns NULL if not successfull
bool SystemEntityKernel::AddSystemEntity(SystemEntityKernel * kernel, std::string name)
{
	if (kernel != 0)
	{
		MyVector3 systemPos = kernel->GetPosition();
		double systemRadius = kernel->GetRadius();
		double distanceBetween = 0.0;
		std::map<MyVector3, SystemEntityKernel*>::iterator kernelfind = _systemPosMap.find(systemPos);
		std::map<MyVector3, EntityKernel*>::iterator loneEntityFind = _loneEntityPosMap.find(systemPos);
		if (kernelfind != _systemPosMap.end() || loneEntityFind != _loneEntityPosMap.end()) //an entity already exists here, try again
		{
			return false;
		}
		BaseEntityKernel * nearestEntity = 0;
		double minDistance = 0;
		double curEntityNearestNeighborDistance = 0;
		BaseEntityKernel *nearestNeighborNearestNeighbor = 0;
		MyVector3 curPos;
		double maxRadius = 0.0;
		double minDistanceBetween = 0;
		BaseEntityKernel * curEntity = 0;
		double row = systemPos.distance(Vector3(0, 0, 0));
		double rowCurPos = 0.0;
		double distanceBetweenRow = 0.0;
		double distanceZ = 0.0;
		//Check for overlapps with other systems
		for (auto it = _systemMap.begin(); it != _systemMap.end(); it++)
		{
			curEntity = it->second;
			if (curEntity != 0)
			{
				curPos = curEntity->GetPosition();
				rowCurPos = curPos.distance(Vector3(0, 0, 0));
				distanceBetweenRow = std::abs(row - rowCurPos);
				maxRadius = std::max(systemRadius, curEntity->GetRadius());
				distanceBetween = (double)systemPos.distance(curPos);
				minDistanceBetween = maxRadius*3;
				distanceZ = abs(curPos[2] - systemPos[2]);
				if (distanceBetween < minDistanceBetween || (distanceZ < minDistanceBetween && distanceBetweenRow < minDistanceBetween))
				{
					return false;
				}
			}
			else return false;
		}

		//Check for overlapps with lone entities
		for (auto it = _loneEntityMap.begin(); it != _loneEntityMap.end(); it++)
		{
			curEntity = it->second;
			if (curEntity != 0)
			{
				curPos = curEntity->GetPosition();
				rowCurPos = curPos.distance(Vector3(0, 0, 0));
				distanceBetweenRow = std::abs(row - rowCurPos);
				maxRadius = std::max(systemRadius, curEntity->GetRadius());
				distanceBetween = systemPos.distance(curPos);
				minDistanceBetween = maxRadius*3;
				distanceZ = abs(curPos[2] - systemPos[2]);
				if (distanceBetween < minDistanceBetween || (distanceZ < minDistanceBetween && distanceBetweenRow < minDistanceBetween))
				{
					return false;
				}
			}
			else return false;
		}
		
		std::string nameUnique = EntityKernelManager::GetUniqueName(name);
		_systemMap[nameUnique] = kernel;
		_systemPosMap[systemPos] = kernel;
		EntityKernelManager::_fullSystemList.push_back(kernel);
		kernel->SetName(nameUnique);
		kernel->SetParentKernel(this);
		EntityKernel * centralEntity = kernel->GetCentralEntity();
		if (centralEntity != NULL)
		{
			centralEntity->SetName(nameUnique);
			EntityKernelManager::AddEntityToGlobal(nameUnique, centralEntity);
			kernel->_loneEntityPosMap[centralEntity->GetPosition()] = centralEntity;
			kernel->_loneEntityMap[nameUnique] = centralEntity;
		}
		return true;
	}
	else return false;
}

OrbitingEntityKernel * SystemEntityKernel::GetCentralEntity()
{
	return _centralEntity;
}

//attempt to place a lone orbiting entity, returns NULL if not successfull
bool SystemEntityKernel::AddOrbitingEntity(OrbitingEntityKernel * kernel, std::string name)
{
	if (kernel != 0)
	{
		MyVector3 ObjectPos = kernel->GetPosition();
		double ObjectRadius = kernel->GetRadius();
		double distanceBetween = 0.0;
		std::map<MyVector3, SystemEntityKernel*>::iterator kernelfind = _systemPosMap.find(ObjectPos);
		std::map<MyVector3, EntityKernel*>::iterator loneEntityFind = _loneEntityPosMap.find(ObjectPos);
		if (kernelfind != _systemPosMap.end() || loneEntityFind != _loneEntityPosMap.end()) //an entity already exists here, try again
		{
			return false;
		}
		BaseEntityKernel * nearestEntity = 0;
		double minDistance = 0;
		double curEntityNearestNeighborDistance = 0;
		BaseEntityKernel *nearestNeighborNearestNeighbor = 0;
		MyVector3 curPos;
		double maxRadius = 0.0;
		double minDistanceBetween = 0;
		BaseEntityKernel * curEntity = 0;
		double row = ObjectPos.distance(Vector3(0, 0, 0));
		double rowCurPos = 0.0;
		double distanceBetweenRow = 0.0;
		double distanceZ = 0.0;
		//Check for overlapps with other systems
		for (auto it = _systemMap.begin(); it != _systemMap.end(); it++)
		{
			curEntity = it->second;
			if (curEntity != 0)
			{
				curPos = curEntity->GetPosition();
				rowCurPos = curPos.distance(Vector3(0, 0, 0));
				distanceBetweenRow = std::abs(row - rowCurPos);
				maxRadius = std::max(ObjectRadius, curEntity->GetRadius());
				distanceBetween = (double)ObjectPos.distance(curPos);
				minDistanceBetween = maxRadius*1.5;
				distanceZ = abs(curPos[2] - ObjectPos[2]);
				if (distanceBetween < minDistanceBetween || (distanceZ < minDistanceBetween && distanceBetweenRow < minDistanceBetween))
				{
					return false;
				}
			}
			else return false;
		}

		//Check for overlapps with lone entities
		for (auto it = _loneEntityMap.begin(); it != _loneEntityMap.end(); it++)
		{
			curEntity = it->second;
			if (curEntity != 0)
			{
				curPos = curEntity->GetPosition();
				rowCurPos = curPos.distance(Vector3(0, 0, 0));
				distanceBetweenRow = std::abs(row - rowCurPos);
				maxRadius = std::max(ObjectRadius, curEntity->GetRadius());
				distanceBetween = (double)ObjectPos.distance(curPos);
				minDistanceBetween = maxRadius*1.5;
				distanceZ = abs(curPos[2] - ObjectPos[2]);
				if (distanceBetween < minDistanceBetween || distanceBetweenRow < minDistanceBetween)
				{
					return false;
				}
			}
			else return false;
		}

		_loneEntityPosMap[ObjectPos] = kernel;
		std::string nameUnique = EntityKernelManager::GetUniqueName(name);
		kernel->SetName(nameUnique);
		kernel->SetParentKernel(this);
		_loneEntityMap[nameUnique] = kernel;
		EntityKernelManager::AddEntityToGlobal(nameUnique, kernel);

		return true;
	}
	else return false;
}

void SystemEntityKernel::FindNearestNeighbors()
{
	//go through lone entities first
	for (auto loneIt = _loneEntityMap.begin(); loneIt != _loneEntityMap.end(); loneIt++)
	{
		loneIt->second->FindNearestNeighbor();
	}

	//then go through system entities
	for (auto systemIt = _systemMap.begin(); systemIt != _systemMap.end(); systemIt++)
	{
		systemIt->second->FindNearestNeighbor();
		systemIt->second->FindNearestNeighbors(); //recursively find nearest neighbors of children
	}
}

void SystemEntityKernel::FindNearestNeighborsThread()
{
	//go through lone entities first
	for (auto loneIt = _loneEntityMap.begin(); loneIt != _loneEntityMap.end(); loneIt++)
	{
		loneIt->second->FindNearestNeighbor();
	}

	//then go through system entities
	for (auto systemIt = _systemMap.begin(); systemIt != _systemMap.end(); systemIt++)
	{
		systemIt->second->FindNearestNeighbor();
	}
}

//adds a brand new orbiting entity to this system
OrbitingEntityKernel *  SystemEntityKernel::AddOrbitingEntity(EntityType type,
	MyVector3 position,
	double mass,
	double radius,
	std::string name)
{
	OrbitingEntityKernel * orbitingEntity = new OrbitingEntityKernel(type,
		position,
		mass,
		radius);

	if (!AddOrbitingEntity(orbitingEntity, name))
	{
		delete orbitingEntity;
		return 0;
	}
	return orbitingEntity;
}

//add an existing entity to this system
void SystemEntityKernel::AddEntity(EntityKernel* kernel, std::string name)
{
	std::string uniqueName = EntityKernelManager::GetUniqueName(name);
	_loneEntityMap[uniqueName] = kernel;
	EntityKernelManager::AddEntityToGlobal(uniqueName, kernel);
	kernel->SetName(uniqueName);
	kernel->SetParentKernel(this);
}

EntityKernel *  SystemEntityKernel::GetEntity(std::string name)
{
	EntityKernel * entity = 0;
	std::map<std::string, EntityKernel*>::iterator itfind = _loneEntityMap.find(name);
	if (itfind != _loneEntityMap.end())
		entity = itfind->second;

	return entity;
}

void SystemEntityKernel::UpdateEntityForTimeStep()
{
	FindNearestNeighborsThread();
	//for now updates the position of all lone entities in this system and the position of this system
	if (_centralEntity != NULL) 
	{
		double yawDirection = (double)_centralEntity->_yawDirection;
		double centralEntityDegSpeedPerTick = _centralEntity->GetOrbitVelocity();
		double yawAngle = yawDirection * centralEntityDegSpeedPerTick;

		Vector3 position(_position[0], _position[1], _position[2]);
		position = _centralEntity->_rotationMatrix*position;

		this->_position_mutex.lock();
		_position = MyVector3(position[0], position[1], position[2]);
		this->_position_mutex.unlock();

		newPositionList.push_back(std::pair<EntityKernel*, MyVector3>(_centralEntity, MyVector3(0,0,0)));
	}

	for (auto it = _loneEntityMap.begin(); it != _loneEntityMap.end(); it++)
	{
		//if(it->second->isCentralEntity()) //for debugging purposes, only render the parent level entities
			it->second->UpdateEntityForTimeStep();
	}
}

void SystemEntityKernel::UpdateFromSystem() //Updates from the thread values in this system, then calls children
{
	for (auto commandIt = _threadCommandQueue.begin(); commandIt != _threadCommandQueue.end(); commandIt++)
	{
		(*commandIt)->Process();
	}

	for (auto it = newPositionList.begin(); it != newPositionList.end(); it++)
	{

		EntityKernel * kernel = it->first;
		MyVector3 newPosition = it->second;
		//Convert it to a world position
		EntityKernelManager::GetDerivedParentPosition(kernel, newPosition);

		//check if this object should be rendered
		Ogre::SceneNode * node = kernel->GetRendererNode();
		if (node != NULL)
		{
			float distCameraNode = EntityKernelManager::_mainApp->mCameraNode->getPosition().distance(node->getPosition());
			if (kernel->_parentEntity == NULL && distCameraNode < kernel->renderDistance)
			{
				Procedural::SphereGenerator().setRadius((Real)kernel->GetRadius()).realizeMesh(kernel->GetName());
				kernel->_parentEntity = EntityKernelManager::GetSceneManager()->createEntity(kernel->GetName());
				kernel->_parentEntity->setMaterial(kernel->_material);
				node->attachObject(kernel->_parentEntity);
			}
			else if(kernel->_parentEntity != NULL && distCameraNode >= kernel->renderDistance)
			{
				node->getCreator()->destroyMovableObject(kernel->_parentEntity);
				MeshManager::getSingleton().remove(kernel->GetName());
				kernel->_parentEntity = NULL;
			}

			node->setPosition(newPosition);

			ParticleSystem * glowSystem = kernel->_glowSystem;
			if (glowSystem != NULL)
			{
				if (glowSystem->getNumParticles() == 0) //need to add glow particle, it hasnt been created yet
				{
					Particle * particle = glowSystem->createParticle();
					particle->mParticleType = Particle::ParticleType::Visual;
					particle->mColour = ColourValue(ColourValue::White);
					particle->setDimensions((Real)kernel->GetRadius() * 5, (Real)kernel->GetRadius() * 5);
				}
			}

			//check if particle systems should be on, only render when close enough
			ParticleSystem * trailSystem = kernel->_trailSystem;
			ParticleSystem * animateTexSys = kernel->_animatedTextureSystem;
			Ogre::Entity * entity = kernel->GetRendererEntity();
			if (entity != NULL) //animations not on yet
			{
				/*
				if (animateTexSys != NULL)
				{
					if (kernel->animatedTexturesOn && entity->isVisible())
					{
						animateTexSys->setEmitting(true);
						std::cout << "Rendering animated texture " << std::endl;
					}
					else
						animateTexSys->setEmitting(false);
				}*/
				if(trailSystem != NULL)
				{
					if (kernel->trailsOn && entity->isVisible())
					{
						trailSystem->setEmitting(true);
					}	
					else
						trailSystem->setEmitting(false);
				}
			}
		}
	}
}

void SystemEntityKernel::GenerateFullSystemListChild()
{
	std::map<std::string, SystemEntityKernel*>::iterator it;
	for (it = _systemMap.begin(); it != _systemMap.end(); it++)
	{
		EntityKernelManager::_fullSystemList.push_back(it->second);
		it->second->GenerateFullSystemListChild();
	}
}
