#ifndef __ENTITYKERNELMANAGER_H__
#define __ENTITYKERNELMANAGER_H__
#include <map>

#include "Timer.h"
#include <random>
#include <thread>
#include <future>
#include "GalaxyTextureGen.h"
#include "ThreadCommand.h"

class SystemEntityKernel;
class SceneManager;
class BaseApplication;

namespace OgreUtils
{
	void destroyAllAttachedMovableObjects(SceneNode* node);

	void destroySceneNode(Ogre::SceneNode* node);

	void destroySceneNodeChildren(Ogre::SceneNode* node);
}


class EntityKernelManager
{

private:
	EntityKernelManager();

	static void DeleteSystemMap();
	static void DeleteEntityMap();

	static double _galaxyRadius;
   	static int _numSystems;
   	static Ogre::SceneManager * _sceneMgr;

public:
	~EntityKernelManager();

	//determines the speed of the simulation, lower = faster
	static int GetNumMicroSecondsPerTick()
	{
		if (_gameSpeedFactor < 0.1) //zerocheck
		{
			return 999999999;
		}
		return (int)((1000.0 * (double)_gameMillisecondsPerTick)/_gameSpeedFactor);
	}

	static bool DoesGalaxyExist()
	{
		return _systemMap.size() > 0 || _entityMap.size() > 0;
	}

	static int GetNumTicksPerGameYear()
	{
		return (int)((10.0*60.0*1000.0) / (double)_gameMillisecondsPerTick); //1 year passes every 10 real time minutes (approximate)
	}

	static void StopRenderingOrbits(OrbitingEntityKernel * centralEntity); //render the orbit paths of this central entity
	static void RenderOrbits(OrbitingEntityKernel * centralEntity);

	static void RenderOrbitPath(OrbitingEntityKernel * entity); //render the orbit path of this entity around its parent
	static void StopRenderOrbitPath(OrbitingEntityKernel * entity); //stop

	static void StopRendereringAllOrbits();

	static int _gameMillisecondsPerTick; //how many game seconds go by every tick
	static double _gameYearsElapsed; //how many game years have elapsed in this simulation
	static double _maxGalaxySpeed;
	static double _minGalaxySpeed;
	static double _gameSpeedFactor;
	static int _tickCount;
	static OrbitingEntityKernel * _centerObject;
	static int seed;

	static double GetGalaxyRadius();
	static double GetCenterObjectRadius();

	static int _numGenerateTries;
	static int _recursionDepth;

	static BaseApplication * _mainApp;

	//nearest neighbor, non thread safe
	static void FindNearestNeighbors();
	//nearest neighbor, thread safe
	static void FindNearestNeighborsThread();

	static void UpdateGameYear() //update each tick
	{
		_gameYearsElapsed = _gameYearsElapsed + (1.0 / (double)GetNumTicksPerGameYear());
	}

	static bool isGeneratingGalaxy;

	static void UpdateOrbitTrailRendering(bool render = false);

	static std::map<std::string, SystemEntityKernel*> _systemMap;
	static std::map<MyVector3, SystemEntityKernel*, VectorLessThan> _systemPosMap; //ordered by pos, only for reference
	static std::map<std::string, EntityKernel*> _loneEntityMap;

	static std::list<SystemEntityKernel*> _fullSystemList;
	static std::list<std::pair<Ogre::SceneNode*, Ogre::SceneNode*> > _orbitSceneNodeList;

	//static bool _renderingOrbits;

	static double GetMaxOrbitVelocity(double orbitRadius)
	{
		return _maxGalaxySpeed / orbitRadius;
	}

	static double GetMinOrbitVelocity(double orbitRadius)
	{
		return _minGalaxySpeed / orbitRadius;
	}

	static double ConvertVelocityToAngularVelocity(double velocity, double radius)
	{
		return velocity / radius;
	}

	static double GetMaxMass(double radius)
	{
		return radius * 2;
	}

	static double GetMinMass(double radius)
	{
		return radius * 1.2;
	}

	static ParticleSystem * _orbitStreamParticleSystem;

	static void RemoveGalaxy();
	static void InitializeParticleSystems();
	static void RemoveMaterials();

	//full map of all kernel entities
	static std::map<std::string, EntityKernel*> _entityMap;

	static Ogre::SceneManager * GetSceneManager() { return _sceneMgr; }
	static void Inititalize(Ogre::SceneManager * sceneMgr, BaseApplication * mainApp);
	static void GenerateGalaxy();
	static std::string GetUniqueName(std::string srcName);
	static void AddEntityToGlobal(std::string name, EntityKernel * kernel)
	{
		_entityMap[name] = kernel;
	}

	static short int _galaxyDirection; //positive or negative

	template<typename T>
	static bool future_is_ready(std::future<T>& t) {
		return t.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
	}

	//thread management
	static std::list<std::future<void>* > _systemThreadList;
	static std::list<ThreadCommand*> _threadCommandQueue;
	static Verse::Timer _timer;

	static bool CheckThreadsIfReady()
	{
		bool allReady = true;
		for (auto it = _systemThreadList.begin(); it != _systemThreadList.end(); it++)
		{
			if (!future_is_ready(**it))
			{
				allReady = false;
				break;
			}
		}
		return allReady;
	}

	static void ClearThreadList()
	{
		for (auto it = _systemThreadList.begin(); it != _systemThreadList.end(); it++)
		{
			(*it)->wait(); //wait for the asyncronous process to finish
			delete (*it);
		}
		_systemThreadList.clear();
	}

	//Only use these in setup;
	static void SetNumSystems(int value) { _numSystems = value; }
	static int GetNumSystems() { return _numSystems; }

	static void UpdateFromSystemThreadOutputs();

	static EntityKernel * GetEntity(std::string name);
	static SystemEntityKernel *GetSystemEntity(std::string name);

	static void ChangeSystemName(std::string orgName, std::string newName);
	static void ChangeEntityName(std::string orgName, std::string newName);
	static void AddKernelEntity(EntityKernel * kernel, std::string name = "");
	static EntityKernel * AddKernelEntity(entitykernel::EntityType type = entitykernel::STAR,
		MyVector3 position = MyVector3(0.0, 0.0, 0.0),
		double mass = 1.0,
		double radius = 1.0,
		bool isStatic = true, 
		std::string name = "");

	static bool AddSystemEntity(SystemEntityKernel * kernel, std::string name = "");
	static SystemEntityKernel * AddSystemEntity(entitykernel::EntityType centerEntitytype = entitykernel::STAR,
		double sysRadius = 1.0,
		MyVector3 centerPos = MyVector3(0.0, 0.0, 0.0),
		double centerMass = 1.0,
		double centerObjectRadius = 1.0,
		std::string name = "");
	
	static bool RemoveSystemEntity(std::string name);
	static bool RemoveKernelEntity(std::string name);
	static bool RemoveKernelEntity(EntityKernel * kernel); //warning, if succ this pointer will no longer be valid, auto deletion
	static bool RemoveSystemEntity(SystemEntityKernel * kernel); //warning, if succ this pointer will no longer be valid, auto deletion

	static void UpdateEntitiesForTimeStep(); //launch all worker threads to update entities

	static void PrintOrderedByPostitionSystemMap();

	static std::default_random_engine _generator;

	//get the derived world position of a child with a relative position
	static void GetDerivedParentPosition(BaseEntityKernel* childKernel, MyVector3 &derivedPos); //pass in the starting kernel with starting pos
};

#endif
