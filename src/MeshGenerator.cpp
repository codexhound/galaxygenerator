#include "MeshGenerator.h"
#include "Procedural.h"
#include <iostream>
#include "MathDefines.h"
#include "EntityKernelManager.h"
using namespace Verse;

float MeshGenerator::_accuracy = 40;

Ogre::Entity * MeshGenerator::DonutEntity(std::string name, float radius, float thickness)
{
	Procedural::Path p3;

	for (float it = 0; it < (float)(2 * M_PI); it  = it + (float)(M_PI/_accuracy))
	{
		p3.addPoint((float)radius*cos(it), (float)radius*sin(it), 0);
	}
	p3.close();
	Procedural::Shape s3;
	for (float itj = 0; itj < (float)(2 * M_PI); itj += (float)(M_PI / 8))
	{
		s3.addPoint((float)thickness*cos(itj), (float)thickness*sin(itj));
	}
	s3.close();

	Procedural::Extruder().setExtrusionPath(&p3).setShapeToExtrude(&s3).realizeMesh(name);
	Ogre::Entity* ent2 = EntityKernelManager::GetSceneManager()->createEntity(name);
	return ent2;
}

Ogre::Entity * MeshGenerator::ExtrudeCircleAlongPath(std::string name, float thickness, std::list<Vector3> &points)
{
	if (!(points.size() > 0))
		return 0;

	auto first = points.begin();
	auto second = ++points.begin();

	float distancePt = (*first).distance((*second));
	if (distancePt < 0.3)
		return 0;

	
	Procedural::Path p3;
	Vector3 point;

	int count = 0;
	Vector3 prevLast;
	Vector3 last = points.back();
	for (auto pathit = points.begin(); pathit != points.end(); pathit++)
	{
		point = (*pathit);
		if(count < points.size()-1)
			p3.addPoint(point[0], point[1], point[2]);

		if (count == points.size() - 2)
			prevLast = point;
		count++;
	}
	float distancePtLast = points.back().distance(prevLast);
	std::cout << "Check 1: Path Size: " << points.size() << ", Point Distance: " << distancePt << ", Back Distance: " << distancePtLast << std::endl;
	std::cout << "First Position: " << points.front() << ", Last Position: " << points.back() << "Second to last Position: " << prevLast << std::endl;
	std::cout << "Distance From First to Last (Connecting): " << points.front().distance(points.back()) << std::endl;
	p3.close();
	std::cout << "Check 2" << std::endl;
	Procedural::Shape s3;
	for (float itj = 0; itj < (float)(2 * M_PI); itj += (float)(M_PI / 8))
	{
		s3.addPoint((float)thickness*cos(itj), (float)thickness*sin(itj));
	}
	s3.close();
	std::cout << "Check 3" << std::endl;
	Procedural::Extruder extrude;
	extrude.setExtrusionPath(&p3).setShapeToExtrude(&s3);
	std::cout << "Check 3.1" << std::endl;
	extrude.realizeMesh(name);
	std::cout << "Check 4" << std::endl;
	Ogre::Entity* ent2 = EntityKernelManager::GetSceneManager()->createEntity(name);
	return ent2;
}
