#include "ThreadCommand.h"
#include "EntityKernel.h"
#include "EntityKernelManager.h"

void OrbitRender::Process()
{
	if(render)
		EntityKernelManager::RenderOrbits(centralEntity);
	else
		EntityKernelManager::StopRenderingOrbits(centralEntity);
}

void OrbitPathRender::Process()
{
	if (render)
		EntityKernelManager::RenderOrbitPath(entity);
	else
		EntityKernelManager::StopRenderOrbitPath(entity);
}

void StopRenderingAllOrbits::Process()
{
	EntityKernelManager::StopRendereringAllOrbits();
}
