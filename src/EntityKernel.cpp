#include "EntityKernel.h"
#include <OgreSceneNode.h>
#include "EntityKernelManager.h"
#include "SystemEntityKernel.h"
#include <iostream>
#include "MeshGenerator.h"
#include "MathDefines.h"
#include "OgreParticleSystem.h"
#include "OgreSceneManager.h"
#include "ThreadContainers.h"
#include "OgreParticle.h"

std::string entitykernel::ConvertEntityTypeToString(EntityType type)
{
	if (type == PLANET)
		return "Planet";
	else if (type == MOON)
		return "Moon";
	else if (type == ASTEROIDBELT)
		return "Asteroid Belt";
	else if (type == STAR)
		return "Star";
	else if (type == BLACKHOLE)
		return "Black Hole";
	else
		return "Unknown";
}

double MyVector3::_tolerance = 0.00003;

bool MyVector3::operator==(const MyVector3 &b) const
{
	bool equals = false;
	bool xEqual = std::abs(b[0] - x) < _tolerance;
	bool yEqual = std::abs(b[1] - y) < _tolerance;
	bool zEqual = std::abs(b[2] - z) < _tolerance;
	if(xEqual && yEqual && zEqual)
	{
		equals = true;
	}
	return equals;
}

bool MyVector3::operator<(const MyVector3 &b) const
{
	bool lessThan = false;
	bool xEqual = std::abs(b[0] - x) < _tolerance;
	bool yEqual = std::abs(b[1] - y) < _tolerance;
	bool zEqual = std::abs(b[2] - z) < _tolerance;
	bool axless = x < (b[0] - _tolerance);
	bool ayless = y < (b[1] - _tolerance);
	bool azless = z < (b[2] - _tolerance);
	if(axless)
	{
		lessThan = true;
	}
	else if(xEqual && ayless)
	{
		lessThan = true;
	}
	else if(xEqual && yEqual && azless)
	{
		lessThan = true;
	}

	return lessThan;
}

void BaseEntityKernel::FindNearestNeighbor()
{
	std::map<std::string, EntityKernel*> *loneEntityMap = &EntityKernelManager::_loneEntityMap;
	std::map<std::string, SystemEntityKernel*> *systemMap = &EntityKernelManager::_systemMap;
	SystemEntityKernel * parentKernel = this->GetParentKernel();

	if (parentKernel != NULL)
	{
		loneEntityMap = &parentKernel->_loneEntityMap;
		systemMap = &parentKernel->_systemMap;
	}

	double minDistance = 0;
	BaseEntityKernel * nnEntity = NULL;
	double distance = 0;
	Vector3 myPos = this->GetPosition();
	Vector3 curPos = Vector3(0, 0, 0);
	BaseEntityKernel * curKernel = NULL;

	if (loneEntityMap != NULL && systemMap != NULL)
	{
		//search through lone EntityMap
		for (auto loneIt = loneEntityMap->begin(); loneIt != loneEntityMap->end(); loneIt++)
		{
			curKernel = loneIt->second;
			if (curKernel != this) //do not compare with yourself
			{
				curPos = curKernel->GetPosition();
				distance = myPos.distance(curPos) - this->GetRadius() - curKernel->GetRadius();
				if (nnEntity == NULL || distance < minDistance)
				{
					minDistance = distance;
					nnEntity = curKernel;
				}
			}
		}

		//search through system Map
		for (auto systemIt = systemMap->begin(); systemIt != systemMap->end(); systemIt++)
		{
			curKernel = systemIt->second;
			if (curKernel != this) //do not compare with yourself
			{
				curPos = curKernel->GetPosition();
				distance = myPos.distance(curPos) - this->GetRadius() - curKernel->GetRadius();
				if (nnEntity == NULL || distance < minDistance)
				{
					minDistance = distance;
					nnEntity = curKernel;
				}
			}
		}
	}
	

	//set nearest neighbor, if one wasnt found, it will be NULL
	this->SetNearestNeighbor(nnEntity);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

EntityKernel::EntityKernel(entitykernel::EntityType type, MyVector3 position,
	double mass, double radius, bool isStatic) : BaseEntityKernel()
{
	_type = type;
	_mass = mass;
	_isStatic = isStatic;
	_radius = radius;
	_position = position;
	_startingPosition = _position;
	_startingTheta = 0;
	_lastTheta = _startingTheta;
	_glowSystem = 0;
	_trailSystem = 0;
	_selectionSystem = 0;
	_animatedTextureSystem = 0;
	trailsOn = false;
	animatedTexturesOn = true;
	isSelected = false;
	renderDistance = 0;
	_parentNode = NULL;
	_parentEntity = NULL;
}

void EntityKernel::InitializeSelectionSystem()
{
	SceneNode * parentNode = GetRendererNode();
	if (parentNode != NULL)
	{
		std::string glowSystemName = _name + "_selection_particleSystem";
		_selectionSystem = EntityKernelManager::GetSceneManager()->createParticleSystem(glowSystemName);
		_selectionSystem->setMaterialName("Selection/Blue");
		_selectionSystem->setKeepParticlesInLocalSpace(true);
		_selectionSystem->setSpeedFactor(0);
		_selectionSystem->setVisible(false);
		parentNode->attachObject(_selectionSystem);
	}
}

void EntityKernel::InitilaizeGlowSystem(std::string material)
{
	//sun glow effect
	SceneNode * parentNode = GetRendererNode();
	if (parentNode != NULL)
	{
		std::string glowSystemName = _name + "_starGlow_particleSystem";
		_glowSystem = EntityKernelManager::GetSceneManager()->createParticleSystem(glowSystemName);
		_glowSystem->setKeepParticlesInLocalSpace(true);
		_glowSystem->setMaterialName(material);
		_glowSystem->setSpeedFactor(0);
		parentNode->attachObject(_glowSystem);
	}
}

EntityKernel::~EntityKernel()
{

}

void EntityKernel::SetParentNode(Ogre::SceneNode * node)
{
	_parentNode = node;
	/*
	_parentEntity = entity;
	if (entity != 0)
	{
		_parentNode = entity->getParentSceneNode();
	}*/
}

void EntityKernel::SetMass(double mass)
{
	_mass = mass;
}

Ogre::SceneNode * EntityKernel::GetRendererNode()
{
	return _parentNode;
}

Ogre::Entity * EntityKernel::GetRendererEntity()
{
	return _parentEntity;
}

double EntityKernel::GetMass()
{
	return _mass;
}

bool EntityKernel::GetIsStatic()
{
	return _isStatic;
}

double EntityKernel::CalculateTheta(double x0, double y0)
{
	double r0 = sqrt(x0 * x0 + y0 * y0);
	double theta0 = atan(y0 / x0);

	if (x0 < 0 && y0 > 0 && theta0 < 0) theta0 = theta0 + M_PI;
	else if (x0 < 0 && y0 < 0 && theta0 > 0) theta0 = theta0 + M_PI;

	return theta0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

//creates the rotation matrix for this orbiting entity and generates the orbit path
void OrbitingEntityKernel::SetOrbitVelocity(double velocity)
{
	_orbitVelocity = velocity;

	MyVector3 point = _position;
	
	if (_isCentralEntity && _parentKernel != NULL)
	{
		point = _parentKernel->GetPosition();
	}

	double x0 = point[0];
	double y0 = point[1];
	double z0 = point[2];

	//make another point to complete the axis of rotation, must have the same length as original position
	double row = point.distance(Vector3(0, 0, 0));
	if (y0 > 0) row = -row;
	Vector3 pt2(0, row, 0);

	//create an axis normal to the two vectors
	Vector3 crossProduct = point.crossProduct(pt2);
	crossProduct.normalise();
	
	_rotationMatrix.FromAngleAxis(crossProduct, Radian(_orbitVelocity));
	Vector3 nextPos = point*_rotationMatrix;

	double theta1 = CalculateTheta(point[0], point[1]);
	double theta2 = CalculateTheta(nextPos[0], nextPos[1]);
	double thetaDiff = theta2 - theta1;
	if ((_yawDirection > 0 && thetaDiff < 0) || (_yawDirection < 0 && thetaDiff > 0)) //not the right direction, reverse
	{
		_orbitVelocity = -_orbitVelocity;
	}

	_rotationMatrix.FromAngleAxis(crossProduct, Radian(_orbitVelocity));
	CalculateOrbitPath(crossProduct);
}

void OrbitingEntityKernel::CalculateOrbitPath(Vector3 crossProduct)
{
	int incr = 200;
	float radianInc = (2 * M_PI) / (float)incr;
	if (_orbitVelocity < 0)
		radianInc *= -1;

	Vector3 pointLocation = _position;
	Ogre::Matrix3 rotationMatrix;
	rotationMatrix.FromAngleAxis(crossProduct, Radian(radianInc));
	if (_isCentralEntity && _parentKernel != NULL)
	{
		pointLocation = _parentKernel->GetPosition();
	}

	std::string orbitPointName = _name + "orbitPoint_" + std::to_string(0);
	_orbitPath.push_back(std::pair<std::string, Vector3>(orbitPointName, pointLocation));

	for (int i = 0; i < incr; i++)
	{
		pointLocation = rotationMatrix * pointLocation;
		orbitPointName = _name + "orbitPoint_" + std::to_string(i+1);
		_orbitPath.push_back(std::pair<std::string, Vector3>(orbitPointName, pointLocation));
	}
}

double OrbitingEntityKernel::GetOrbitVelocity()
{
	return _orbitVelocity;
}


//gets the next position for this entity
void OrbitingEntityKernel::UpdateEntityForTimeStep()
{
	if (!_isCentralEntity && _parentKernel != NULL)
	{
		Vector3 position(_position[0], _position[1], _position[2]);
		position = _rotationMatrix*position;

		this->_position_mutex.lock();
		_position = MyVector3(position[0], position[1], position[2]);
		this->_position_mutex.unlock();

		SystemEntityKernel * parentSystem = dynamic_cast<SystemEntityKernel*>(_parentKernel);
		if (parentSystem != NULL)
			parentSystem->newPositionList.push_back(std::pair<EntityKernel*, MyVector3>(this, _position));
	}
}

void OrbitingEntityKernel::SetOrbitParent(OrbitingEntityKernel * orbitParent)
{
	_orbitParent = orbitParent;
	if (_orbitParent != NULL)
	{
		Ogre::SceneNode * orbitNode = _orbitParent->GetRendererNode();
		if (orbitNode != NULL)
		{
			_orbitSceneNode = new SceneNode(EntityKernelManager::GetSceneManager());
			_orbitSceneNode->setUserAny(Ogre::Any(this));
		}
	}
}
