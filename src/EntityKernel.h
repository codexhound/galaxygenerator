#pragma once
#ifndef __ENTITYKERNEL_H__
#define __ENTITYKERNEL_H__

#include <OgreVector3.h>
#include <OgreEntity.h>
#include <mutex>

using namespace Ogre;

class MyVector3 : public Ogre::Vector3
{
	public:
		MyVector3() : Vector3() {}
		MyVector3(Real x, Real y, Real z) : Vector3(x,y,z) {}
		bool operator<(const MyVector3 &b) const;
		void SetTolerance(double tolerance) {
			_tolerance = tolerance;
		}
		bool operator==(const MyVector3 &b) const;


	private:
		static double _tolerance;
};



class SystemEntityKernel;

namespace entitykernel
{
	enum EntityType
	{
		PLANET,
		ASTEROIDBELT,
		STAR,
		BLACKHOLE,
		MOON
	};

	enum TypeDefs
	{
		SYSTEMCENTER,
		ORBIT,
		NOTYPE
	};

	enum KernelType
	{
		SYSTEM,
		OBJECT
	};
	struct VectorLessThan
	{
		bool operator() (const MyVector3 &a, const MyVector3 &b) const
		{
			return a < b;
		}
	};

	std::string ConvertEntityTypeToString(EntityType type);
}

using namespace entitykernel;

class Entity;
class Node;

class BaseEntityKernel
{
	public:
		BaseEntityKernel() 
		{ 
			_nearestNeighbor = 0; 
			_isCentralEntity = false;
			_parentKernel = 0;
			_name = "";
		}

		BaseEntityKernel * GetNearestNeighbor() { return _nearestNeighbor; }
		MyVector3 GetPosition() { return _position; }
		double GetRadius() { return _radius; }
		std::string GetName() { return _name; }
		bool isCentralEntity() { return _isCentralEntity; }
		SystemEntityKernel * GetParentKernel() { return _parentKernel; }

		void SetNearestNeighbor(BaseEntityKernel * kernel) { _nearestNeighbor = kernel; }
		void SetName(std::string name) { _name = name; }
		void SetRadius(double radius) { _radius = radius; }
		void SetPosition(MyVector3 position) { _position = position; }
		void SetisCentralEntity(bool value) { _isCentralEntity = value; }
		void SetParentKernel(SystemEntityKernel * value) { _parentKernel = value; }

		entitykernel::EntityType GetType()
		{
			return _type;
		}

		void SetType(entitykernel::EntityType type)
		{
			_type = type;
		}

		virtual void UpdateEntityForTimeStep() = 0;

		MyVector3 _startingPosition;
		double _startingTheta;
		double _lastTheta;

		void FindNearestNeighbor();
	
	protected:
		std::mutex _position_mutex;  // locks _position
		MyVector3 _position; //this position is always relative to the parents position
		double _radius;
		std::string _name;
		BaseEntityKernel * _nearestNeighbor; //nearest object to this in the parent scope
		bool _isCentralEntity;
		SystemEntityKernel * _parentKernel;
		entitykernel::EntityType _type;
};

class EntityKernel : public BaseEntityKernel
{
	public:
		EntityKernel(entitykernel::EntityType type = EntityType::STAR, MyVector3 position = MyVector3(0.0, 0.0, 0.0), 
			double mass = 1.0, double radius = 1.0, 
			bool isStatic = true);
		~EntityKernel();
		void SetParentNode(Ogre::SceneNode * node); //sets the parent entity along with its parent node
		void SetMass(double mass);

		virtual void UpdateEntityForTimeStep() = 0;

		Ogre::SceneNode * GetRendererNode();
		Ogre::Entity * GetRendererEntity();
		double GetMass();
		bool GetIsStatic();

		double CalculateTheta(double x0, double y0);

		ParticleSystem * _glowSystem;
		ParticleSystem * _trailSystem;
		bool trailsOn;
		ParticleSystem * _animatedTextureSystem;
		bool animatedTexturesOn;
		ParticleSystem * _selectionSystem;
		double renderDistance; //distance at which this will not be rendered
		Ogre::Entity * _parentEntity;
		Ogre::MaterialPtr _material;
		bool isSelected;

		void InitializeSelectionSystem();
		void InitilaizeGlowSystem(std::string material);


	protected:
		Ogre::SceneNode * _parentNode;
		double _mass;
		bool _isStatic; //is this object affected by the gravity of other entities
};

//Oribiting Entity
//This class is not intended for independent usage, use AddEntity in SystemEntityKernel Instead
class OrbitingEntityKernel : public EntityKernel
{
public:
	OrbitingEntityKernel(entitykernel::EntityType type = EntityType::STAR, MyVector3 position = MyVector3(0.0, 0.0, 0.0),
		double mass = 1.0, double radius = 1.0,
		bool isStatic = true) : EntityKernel(type, position, mass, radius, isStatic)
	{
		_orbitVelocity = 0.0;
		_yawDirection = -1;
		_orbitParent = 0;
		_orbitSceneNode = 0;
		_numOrbitChildren = 0;
		_totalOrbitTravel = 0;
		_orbitRendered = false;
		_orbitsRendered = false;
	}
	Ogre::SceneNode * _orbitSceneNode;
	void SetOrbitVelocity(double velocity);
	double GetOrbitVelocity();

	double _totalOrbitTravel;
	void UpdateEntityForTimeStep();

	short int _yawDirection;

	int _numOrbitChildren;

	void SetOrbitParent(OrbitingEntityKernel * orbitParent);

	Ogre::Matrix3 _rotationMatrix;

	std::list<std::pair<std::string, Ogre::Vector3> > _orbitPath;
	OrbitingEntityKernel * _orbitParent;

	bool _orbitRendered;
	bool _orbitsRendered;

private:
	double _orbitVelocity;

	void CalculateOrbitPath(Vector3 crossProduct);
};

#endif

// End of File